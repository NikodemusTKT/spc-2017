package tehtava74;

import javafx.event.ActionEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class MainController {
	@FXML
    private Label TextLabel;
    @FXML
    private TextField inputField;
    @FXML
    private VBox vbox;
    @FXML
    private Button inputButton;

    @FXML
    public void initialize() {
        inputField.setOnKeyPressed( e -> {
            if (e.getCode() == KeyCode.ENTER)
                ButtonAction();
        });
        inputButton.setOnAction(e -> ButtonAction());
        // inputField.setOnKeyPressed(this::keyPressed);
    }


    @FXML
	public void ButtonAction(){
        TextLabel.setText(TextLabel.getText() + inputField.getText());
        inputField.setText("");
	}
    @FXML
    public void keyPressed(KeyEvent event) {
    	inputField.textProperty().addListener((observable, oldValue, newValue) -> {
    	    TextLabel.setText(inputField.getText());
    	});
    }

}
