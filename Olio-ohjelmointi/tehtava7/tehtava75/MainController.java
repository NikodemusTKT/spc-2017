package tehtava75;

import java.awt.event.WindowAdapter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOError;
import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;


public class MainController {
	@FXML
	private TextField area;

	@FXML
	private TextArea textarea;
	@FXML
	private Button Loadbutton, Savebutton, Exitbutton;
    @FXML
    private VBox vbox;

	@FXML
	private void initialize() {
		area.setPromptText("Give filename");
	}

	@FXML
	private void readFile() throws IOException {
        String fileinput = area.getText();
        if (fileinput.isEmpty()) {

            Alert inputalert = new Alert(AlertType.ERROR);
            inputalert.setHeaderText("You didn't give filename to load!");
            inputalert.setTitle("Load Error");
            inputalert.showAndWait();
        } else {
            BufferedReader input = null;
            try {
                input = new BufferedReader(new FileReader(fileinput));
                StringBuilder sb = new StringBuilder();
                String line;
                while ((line = input.readLine()) != null) {
                    sb.append(line+"\n");
                } 
                textarea.setText(sb.toString());
                area.clear();
            }
            catch (FileNotFoundException e) {
                Alert loadAlert = new Alert(AlertType.ERROR);
                loadAlert.setHeaderText("Given filename was not found.");
                loadAlert.setContentText("Please enter new filename.");
                loadAlert.setTitle("Load Error");
                loadAlert.showAndWait();
                area.requestFocus();
            } finally {
                if (input != null) {
                    input.close();
                }
            }
        }
        area.clear();
    }
    @FXML
    private void saveFile() throws IOException {
        String fileoutput = area.getText();
        if (fileoutput.isEmpty()) {
            Alert outputalert = new Alert(AlertType.ERROR);
            outputalert.setHeaderText("You didn't give filename to save!");
            outputalert.setTitle("Load Error");
            outputalert.showAndWait();
        } else {
            BufferedWriter output = null;
            try {
                output = new BufferedWriter(new FileWriter(fileoutput));
                output.write(textarea.getText().toString());
                } 
            catch (IOError e) {
                Alert loadAlert = new Alert(AlertType.ERROR);
                loadAlert.setHeaderText("File couldn't have been saved.");
                loadAlert.setTitle("Save Error");
                loadAlert.showAndWait();
            } finally {
                if (output != null) {
                    output.close();
                }
            }
        }
        area.clear();
    }
    @FXML
    private void exit() {
        System.exit(0);
    }
}
