package tehtava73;
import javafx.event.ActionEvent;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

public class MainController {
	@FXML
    private Label TextLabel;
    @FXML
    private TextField inputField;
    @FXML
	public void ButtonAction(ActionEvent buttonpress){
        TextLabel.setText(inputField.getText());
        inputField.clear();
	}
    @FXML
    public void keyPressed(KeyEvent event) {
    	if (event.getCode().compareTo(KeyCode.ENTER)==0) {
    		TextLabel.setText(inputField.getText());
    
    	}
    }
	
	
}
