/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 01.06.17                                                              ****/
/**** Exercise: 4-5                                                               ****/
/*************************************************************************************/
package tehtava45;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ReadAndWriteIO {
    public void ReadAndWrite(String infile) {
        String line;
        ZipInputStream input = null;
        try {
            input = new ZipInputStream(new FileInputStream(infile));
            ZipEntry zip;
            while ((zip = input.getNextEntry()) != null) {
                byte[] buffer = new byte[(int) zip.getSize()];
                int size = input.read(buffer);
                line = new String(buffer,0,size);
                System.out.println(line);

                }
            }
        catch (FileNotFoundException ex) {
            System.out.println("File was not found!");
            System.out.println(System.getProperty("user.dir"));
        } catch (IOException e) {
            System.out.println("File wasn't able to be read");
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
            }
            catch (IOException ex) {
                System.out.println("Files could not be closed!");
            }
        }
    }
}


