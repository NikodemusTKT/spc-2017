/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 01.06.17                                                              ****/
/**** Exercise: 4-2                                                               ****/
/*************************************************************************************/



package tehtava42;

public class Mainclass {
    public static void main (String[] args) {
       ReadAndWriteIO rw = new ReadAndWriteIO(); 
       rw.ReadAndWrite("input.txt", "output.txt");
    }
}

