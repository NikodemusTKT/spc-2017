/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 01.06.17                                                              ****/
/**** Exercise: 4-1                                                               ****/
/*************************************************************************************/
package tehtava41;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ReadAndWriteIO {
    private String line;
    public void readFile(String filepath) {
        try {
            BufferedReader input = new BufferedReader(new FileReader(filepath));
            while ((line = input.readLine()) != null) {
                System.out.println(line);
            }
            if (input != null) {
                input.close();
            }
        }
        catch (FileNotFoundException ex) {
            System.out.println("File was not found!");
            System.out.println(System.getProperty("user.dir"));
        }
        catch (IOException e) {
            System.out.println("File wasn't able to be read");
        }
    }
}

                    
