/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 01.06.17                                                              ****/
/**** Exercise: 4-1                                                               ****/
/*************************************************************************************/



package tehtava41;

public class Mainclass {
    public static void main (String[] args) {
       ReadAndWriteIO r = new ReadAndWriteIO(); 
       r.readFile("input.txt");
    }
}

