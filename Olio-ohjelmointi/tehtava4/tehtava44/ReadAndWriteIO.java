/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 01.06.17                                                              ****/
/**** Exercise: 4-3                                                               ****/
/*************************************************************************************/
package tehtava44;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadAndWriteIO {
    public void ReadAndWrite(String infile, String outfile) {
        String line;
        BufferedReader input = null;
        BufferedWriter output = null;
        try {
            input = new BufferedReader(new FileReader(infile));
            output = new BufferedWriter(new FileWriter(outfile));
            while ((line = input.readLine()) != null) {
                if (line.length() < 30 && !line.trim().isEmpty()) {
                    if (line.contains("v"))  {
                        output.write(line+"\n");
                        break;
                    }
                }
            }
        }
        catch (FileNotFoundException ex) {
            System.out.println("File was not found!");
            System.out.println(System.getProperty("user.dir"));
        } catch (IOException e) {
            System.out.println("File wasn't able to be read");
        } finally {
            try {
                if (input != null) {
                    input.close();
                }
                if (output != null) {
                    output.close();
                } 
            }
            catch (IOException ex) {
                System.out.println("Files could not be closed!");
            }
        }
    }
}


