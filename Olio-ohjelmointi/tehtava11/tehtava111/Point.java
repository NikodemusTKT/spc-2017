/**
 * 
 */
package tehtava111;

import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;

/**
 * @author tkt
 *
 */
public class Point {
    private String name = "circle";
    private Circle circle;

    /**
     * @param name
     * @param circle
     */
    public Point(String name) {
        circle = new Circle();
        this.name = name;
    }

    /**
     *
     */
    public Point() {}

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the circle
     */
    public Circle getCircle() {
        return circle;
    }

    public Circle createPoint(MouseEvent event, Pane rectangle) {
        ShapeHandler sh = ShapeHandler.getInstance();
        circle.setCenterX(event.getX());
        circle.setCenterY(event.getY());
        circle.setRadius(10);
        if (circle.getCenterX() - rectangle.getLayoutBounds().getMinX() < 10) {
            circle.setCenterX(rectangle.getLayoutBounds().getMinX() + 10);
        } else if (circle.getCenterY() - rectangle.getLayoutBounds().getMinY() < 10) {
            circle.setCenterY(rectangle.getLayoutBounds().getMinY() + 10);
        } else if (rectangle.getLayoutBounds().getMaxX()
                - circle.getCenterX() < 10) {
            circle.setCenterX(rectangle.getLayoutBounds().getMaxX() - 10);
        } else if (rectangle.getLayoutBounds().getMaxY()
                - circle.getCenterY() < 10) {
            circle.setCenterY(rectangle.getLayoutBounds().getMaxY() - 10);
                }
        circle.setOnMouseClicked( e -> {
            sh.getEndPoints(rectangle,circle);
            System.out.println("Hei, olen piste!");

        });
        return circle;
    }





}
