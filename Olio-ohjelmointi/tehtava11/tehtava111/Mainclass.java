package tehtava111;
/**
 * @author tkt
 *
 */

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class Mainclass extends Application{
    public static Scene skene;
    /**
     * @return the primaryStage
     */
    public static Scene getScene() {
        return Mainclass.skene;
    }
        

    @Override
    public void start(Stage primaryStage) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Map.fxml"));
            Parent root = loader.load();
            MapController controller = (MapController)loader.getController();
            String image = getClass().getResource("Suomen-kartta.jpg").toExternalForm(); 
            root.setStyle(
                    "-fx-background-image: url('" + image + "');  " +
                    "-fx-background-position: center center; " +
                    "-fx-background-repeat: stretch;" );
            Scene scene = new Scene(root);
            controller.setSceneEvent(scene);
            primaryStage.setTitle("Kartta");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String[] args) {
        launch(args);
    }

}
