package tehtava111;
/**
 * 
 */


import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

/**
 * @author tkt
 *
 */

public class Map implements Initializable {

    private ArrayList<String> siteHistory;
    private int location;
    @FXML
    private Button exitButton;
    @FXML
    private Button backButton;
    @FXML
    private Button nextButton;
    @FXML
    private Button prevButton;
    @FXML
    private Button openButton;
    @FXML
    private Button loadButton;
    @FXML
    private Button refreshButton;
    @FXML
    private Button shotButton;
    @FXML
    private Button iniButton;
    @FXML
    private TextField addressBar;
    @FXML
    private WebView webview;
    @FXML
    private WebEngine engine;
    @FXML
    private AnchorPane pane;
    final KeyCombination exitComb = new KeyCodeCombination(KeyCode.W, KeyCombination.CONTROL_DOWN);
    @FXML
    final KeyCombination focusComb = new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN);
    @FXML
    final KeyCombination prevComb = new KeyCodeCombination(KeyCode.LEFT, KeyCombination.ALT_DOWN);
    @FXML
    final KeyCombination nextComb = new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.ALT_DOWN);
    @FXML
    final KeyCombination refComb = new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN);
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        siteHistory = new ArrayList<String>();
        engine = webview.getEngine();
        engine.load("http://www.google.fi");
        pane.addEventHandler(KeyEvent.KEY_RELEASED, (KeyEvent event) -> {
            if (exitComb.match(event)) {
                Platform.exit();
            }
            if (focusComb.match(event)) {
                addressBar.requestFocus();
            }
            if (prevComb.match(event)) {
                previous();
            }
            if (nextComb.match(event)) {
                next();
            }
            if (refComb.match(event)) {
                refreshSite();
            }
        });

    }

    public void loadSite() throws MalformedURLException{
        String url = addressBar.getText();
        String current = engine.getLocation();
        if (siteHistory.isEmpty()) {
            siteHistory.add(current);
        }
        if (url != null) {
            if (url.equals("index.html")) {
                File file = new File("/home/tkt/git/spc-2017/it-kesaleiri-2017/index2.html");
                URL fileUrl = file.toURI().toURL();
                url = fileUrl.toString();
            }
            else if (!url.startsWith("http://")) {
                url = "http://" + url;
            }
            engine.load(url);
            addressBar.setText(engine.getLocation());
            if (siteHistory.size() < 10) {
                siteHistory.add(engine.getLocation());
            } else {
                for (Iterator<String> itr = siteHistory.iterator(); itr.hasNext();) {
                    siteHistory.remove(itr.next());
                    if (siteHistory.size() < 10) {
                        siteHistory.add(engine.getLocation());
                        break;
                    }
                }
            }

        }
        location = siteHistory.lastIndexOf(engine.getLocation());
    }

    public void EnterLoadSite(KeyEvent event) throws MalformedURLException {
        if (event.getCode() == KeyCode.ENTER) loadSite();
    }

    public void refreshSite() {
        engine.reload();
    }
    public void scriptIni() {
        if (engine != null) engine.executeScript("initialize()");
    }
    public void scriptShot() {
        if (engine != null) engine.executeScript("document.shoutOut()");
    }

    public void exit() {
        Platform.exit();
    }
    public void previous() {
        if (siteHistory != null) {
            if (location > 0) {
                location -= 1;
                String prevURL = siteHistory.get(location);
                if (prevURL != null) {
                    addressBar.setText(prevURL);
                    engine.load(prevURL);
                }
            }
        }
    }
    public void next() {
        if (siteHistory != null) {
            if (location < siteHistory.size()-1) {
                location += 1;
                String nextURL = siteHistory.get(location);
                if (nextURL != null) {
                    addressBar.setText(nextURL);
                    engine.load(nextURL);
                }
            }
        }
    }

}

