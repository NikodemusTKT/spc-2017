/**
 * 
 */
package tehtava111;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 * @author tkt
 *
 */
public class MapController implements Initializable {
    private int i = 1;
    @FXML
    private StackPane base;
    @FXML
    private ShapeHandler sh = ShapeHandler.getInstance();
    @FXML
    private Stage stage;
    @FXML
    private Pane rectangle;
    @Override
    public void initialize ( URL url, ResourceBundle rb) {
        rectangle = new Pane();
        base.setPrefSize(677, 1024);
        base.setMaxSize(677, 1024);
        rectangle.setPrefSize(677,  1024);
        rectangle.setStyle("-fx-border-color: black;");
        rectangle.setMaxSize(677,1024);
        base.setAlignment(Pos.CENTER);
        rectangle.setOnMouseClicked(e -> {
            if (sh.getCircleList().isEmpty()) i = 1;
            if (e.getButton() == MouseButton.SECONDARY) sh.setMany(true);
            if (e.getButton() == MouseButton.PRIMARY) sh.setMany(false);
            if (e.getTarget().equals(rectangle) && e.getButton() != MouseButton.MIDDLE) {
                Point point = new Point("Pallo" + i);
                rectangle.getChildren().add(point.createPoint(e,rectangle));
                sh.addCircletoList(point);
                i++;
            }
        });
        base.getChildren().addAll(rectangle);
    }

    void setSceneEvent(Scene scene) {
        scene.addEventHandler(KeyEvent.KEY_PRESSED, key -> {
            if (key.getCode() == KeyCode.P) {
                for (Point point: sh.getCircleList()) { 
                    double X = point.getCircle().getCenterX();
                    double Y = point.getCircle().getCenterY();
                    String name = point.getName();
                    System.out.printf("Nimi: %s X: %.2f Y: %.2f\n", name,X,Y);
                }
                    
            }
            if (key.getCode() == KeyCode.D)
            {
                sh.destroyShapes(rectangle);
            }
        });
    }
}
