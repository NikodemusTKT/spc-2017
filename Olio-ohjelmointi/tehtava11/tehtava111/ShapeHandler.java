/**
 * 
 */
package tehtava111;

import java.util.ArrayList;

import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;

/**
 * @author tkt
 *
 */
public class ShapeHandler {
    private ArrayList<Point> circleList;
    private ArrayList<Line> lineList;
    private Line lastline = null;
    private Circle circle1 = null; 
    private Circle circle2 = null;
    private boolean many = true;
    private static ShapeHandler instance = null;

    /**
     *
     */
    private ShapeHandler() {
        circleList = new ArrayList<Point>();
        lineList = new ArrayList<Line>();
    }
    /** 
     *
     * Lazy initalization of singleton instance
     * @return the instance
     */
    static public ShapeHandler getInstance() {
        if (instance == null)
        {
            instance = new ShapeHandler();
        }
        return instance;
    }


    /**
     * @return the circleList
     */
    public ArrayList<Point> getCircleList() {
        return circleList;
    }

    /**
     * @return the lineList
     */
    public ArrayList<Line> getLineList() {
        return lineList;
    }

    /**
     * @param many the many to set
     */
    public void setMany(boolean many) {
        this.many = many;
    }

    public void addCircletoList(Point point) {
        circleList.add(point);
    }
    public void getEndPoints(Pane root, Circle circle) {
        if (circle1 == null) {
            circle1 = circle;
            circle1.setFill(Color.BLUE);
        } else if (circle1 != circle) {
            circle2 = circle;
            drawLines(root,circle1,circle2);
        }
            else {
                circle1.setFill(Color.BLACK);
                circle1 = null;
            }
    }


    public void drawLines(Pane root, Circle circleA, Circle circleB) {
        Line line = new Line(circleA.getCenterX(), circleA.getCenterY(),circleB.getCenterX(),circleB.getCenterY());
        line.setStrokeWidth(5);
        line.setStroke(Color.BLACK);
        line.setMouseTransparent(true);
        root.getChildren().add(line);
        circle1.setFill(Color.BLACK);
        circle1 = null;
        circle2 = null;
        if (!many) {
            root.getChildren().remove(lastline); 
            lineList.remove(lastline); 
            lastline = line;
        }
        lineList.add(line);
    }

    public void destroyShapes(Pane root) {
        root.getChildren().removeAll(lineList);
        for (Point point: getCircleList()) { 
            root.getChildren().remove(point.getCircle());
        }
        lineList.clear();
        circleList.clear();
    }





}
