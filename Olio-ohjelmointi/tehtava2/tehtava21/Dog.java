package tehtava21;

public class Dog {
	// Dog rakentaja koiran nimen tulostamiseen
	public Dog(String nimi) {
		System.out.println("Hei, nimeni on " + nimi + "!");
	}
	// Speak metodi lausahduksen tulostukseen
	public void Speak(String lausahdus) {
		System.out.println(lausahdus);
	}
}
