// Tekijä: Teemu Tanninen
// Op.numero: 0508505
// Pvm: 30.05.17

package tehtava22;


public class Mainclass {
	public static void main(String[] args) {
		Dog d1 = new Dog("Rekku");
		Dog d2 = new Dog("Musti");
		d1.Speak("Hau!");
		d2.Speak("Vuh!");
	}
}
