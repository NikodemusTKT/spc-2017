// Tekijä: Teemu Tanninen
// // Op.numero: 0508505
// // Pvm: 30.05.17

package tehtava22;

public class Dog {
    private String nimi;
	// Dog rakentaja koiran nimen tulostamiseen
	public Dog(String knimi) {
        nimi = knimi;
		System.out.println("Hei, nimeni on " + nimi + "!");
	}
	// Speak metodi lausahduksen tulostukseen
	public void Speak(String lausahdus) {
		System.out.println(nimi + ": " + lausahdus);
	}
}
