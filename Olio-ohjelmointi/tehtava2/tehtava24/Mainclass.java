// Tekijä: Teemu Tanninen
// Op.numero: 0508505
// Pvm: 30.05.17

package tehtava24;
import java.util.Scanner;


public class Mainclass {
	public static void main (String[] args) {
        // Koiran nimen tulostus.
        Scanner in = new Scanner(System.in);
        String knimi;
        System.out.print("Anna koiralle nimi: ");
        knimi = in.nextLine();
		Dog d1 = new Dog(knimi);


        // Koiran sanomiset
        String lause;
        System.out.print("Mitä koira sanoo: ");
        lause = in.nextLine();
        d1.Speak(lause);
        in.close();
	}
}
