// Tekijä: Teemu Tanninen
// // Op.numero: 0508505
// // Pvm: 30.05.17

package tehtava24;
import java.util.Scanner;

public class Dog {
    private String nimi;
    // Dog rakentaja koiran nimen tulostamiseen
    public Dog(String knimi) {
        nimi = knimi;
        if (nimi.trim().isEmpty())  {
            nimi = "Doge";
        }
        System.out.println("Hei, nimeni on " + nimi + "!");
    }
    // Speak metodi lausahduksen tulostukseen
    public void Speak(String lausahdus) {
        // While silmukka, jota toistetaan kunnes annettu merkkijono ei ole tyhjä.
        while (lausahdus.trim().isEmpty()) {
            lausahdus = "Much wow!";
            System.out.println(nimi + ": " + lausahdus);
            Scanner input = new Scanner(System.in);
            System.out.print("Mitä koira sanoo: ");
            lausahdus = input.nextLine();
            // Syötevirta suljetaan vain, jos merkkijono ei ole tyhjä
            if(!lausahdus.trim().isEmpty()) {
                input.close();
            }
        }
        System.out.println(nimi + ": " + lausahdus);
    }

}
