// Tekijä: Teemu Tanninen
// Op.numero: 0508505
// Pvm: 30.05.17

package tehtava23;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Mainclass {
	public static void main(String[] args) throws IOException {
        BufferedReader in;
        in = new BufferedReader(new InputStreamReader(System.in));
        String knimi;
        System.out.print("Anna koiralle nimi: ");
        knimi = in.readLine();
		Dog d1 = new Dog(knimi);


        String lause;
        System.out.print("Mitä koira sanoo: ");
        lause = in.readLine();
		d1.Speak(lause);
	}
}
