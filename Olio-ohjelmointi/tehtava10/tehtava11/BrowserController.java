/**
 * 
 */
package tehtava11;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.WebView;

/**
 * @author tkt
 *
 */

public class BrowserController implements Initializable {

    @FXML
    private Button backButton;
    @FXML
    private Button nextButton;
    @FXML
    private Button prevButton;
    @FXML
    private Button openButton;
    @FXML
    private Button loadButton;
    @FXML
    private TextField addressBar;
    @FXML
    private WebView webview;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        webview.getEngine().load("http://www.google.fi");
    }

    public void loadSite() {
        String url = addressBar.getText();
        if (!url.startsWith("www."))  url = "www." + url;
        if (!url.startsWith("http://")) url = "http://" + url;
        addressBar.setText(url);
        webview.getEngine().load(url);
    }

    public void EnterLoadSite(KeyEvent event) {
        if (event.getCode() == KeyCode.ENTER) {
            loadSite();
        }
    }

}

