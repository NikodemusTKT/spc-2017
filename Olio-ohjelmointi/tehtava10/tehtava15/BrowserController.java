/**
 * 
 */
package tehtava15;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.concurrent.Worker;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;


/**
 * @author tkt
 *
 */

public class BrowserController implements Initializable {

    private ArrayList<String> siteHistory;
    private int location;
    @FXML
    private Button exitButton;
    @FXML
    private Button backButton;
    @FXML
    private Button nextButton;
    @FXML
    private Button prevButton;
    @FXML
    private Button openButton;
    @FXML
    private Button loadButton;
    @FXML
    private Button refreshButton;
    @FXML
    private Button shotButton;
    @FXML
    private Button iniButton;
    @FXML
    private TextField addressBar;
    @FXML
    private WebView webview;
    @FXML
    private WebEngine engine;
    @FXML
    private AnchorPane pane;
    @FXML
    private Stage primaryStage = Mainclass.returnStage();
    final KeyCombination exitComb = new KeyCodeCombination(KeyCode.W, KeyCombination.CONTROL_DOWN);
    @FXML
    final KeyCombination focusComb = new KeyCodeCombination(KeyCode.L, KeyCombination.CONTROL_DOWN);
    @FXML
    final KeyCombination prevComb = new KeyCodeCombination(KeyCode.LEFT, KeyCombination.ALT_DOWN);
    @FXML
    final KeyCombination nextComb = new KeyCodeCombination(KeyCode.RIGHT, KeyCombination.ALT_DOWN);
    @FXML
    final KeyCombination refComb = new KeyCodeCombination(KeyCode.R, KeyCombination.CONTROL_DOWN);
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        siteHistory = new ArrayList<String>();
        engine = webview.getEngine();
        engine.load("http://www.google.fi");
        pane.addEventHandler(KeyEvent.KEY_RELEASED, event -> {
            if (exitComb.match(event)) {
                Platform.exit();
            }
            if (focusComb.match(event)) {
                addressBar.requestFocus();
            }
            if (prevComb.match(event)) {
                previous();
            }
            if (nextComb.match(event)) {
                next();
            }
            if (refComb.match(event)) {
                refreshSite();
            }
        });
        engine.getLoadWorker().stateProperty().addListener((ov,oldState,newState) -> {
            if (newState == Worker.State.SUCCEEDED)  {
                primaryStage.setTitle(engine.getLocation());
                addressBar.setText(engine.getLocation());
            }
        });

    }

    public void loadSite() throws MalformedURLException{
        if (siteHistory.isEmpty()) {
            siteHistory.add(engine.getLocation());
        }
        String url = addressBar.getText();
        if (url != null) {
            if (url.equals("index.html")) {
                File file = new File("/home/tkt/git/spc-2017/it-kesaleiri-2017/index2.html");
                URL fileUrl = file.toURI().toURL();
                url = fileUrl.toString();
            }
            else if (!url.startsWith("http://")) {
                url = "http://" + url;
            }
            engine.load(url);
            siteHistory();
        }
    }

    public void siteHistory() {
        if (siteHistory.size() < 10) {
            siteHistory.add(engine.getLocation());
        } else {
            for (String item: siteHistory) {
                siteHistory.remove(item);
                if (siteHistory.size() < 10) {
                    siteHistory.add(engine.getLocation());
                    break;
                }
            }
        }
        location = siteHistory.lastIndexOf(engine.getLocation());
    }

    public void EnterLoadSite(KeyEvent event) throws MalformedURLException {
        if (event.getCode() == KeyCode.ENTER) loadSite();
    }

    public void refreshSite() {
        engine.reload();
    }
    public void scriptIni() {
        if (engine != null) engine.executeScript("initialize()");
    }
    public void scriptShot() {
        if (engine != null) engine.executeScript("document.shoutOut()");
    }

    public void exit() {
        Platform.exit();
    }
    public void previous() {
        if (siteHistory != null) {
            if (location > 0) {
                location -= 1;
                String prevURL = siteHistory.get(location);
                if (prevURL != null) {
                    addressBar.setText(prevURL);
                    engine.load(prevURL);
                }
            }
        }
    }
    public void next() {
        if (siteHistory != null) {
            if (location < siteHistory.size()-1) {
                location += 1;
                String nextURL = siteHistory.get(location);
                if (nextURL != null) {
                    addressBar.setText(nextURL);
                    engine.load(nextURL);
                }
            }
        }
    }

}

