////Testi
//Testi
/**
 * 
 */
package tehtava15;

/**
 * @author tkt
 *
 */

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;


public class Mainclass extends Application{
    private static Stage stage;
    @Override
    public void start(Stage primaryStage) {
        try {
            setStage(primaryStage);
            Parent root = FXMLLoader.load(getClass().getResource("Browser.fxml"));
            Scene scene = new Scene(root);
            scene.getStylesheets().add(getClass().getResource("Browser.css").toExternalForm());
            primaryStage.setTitle("Internet selain");
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        launch(args);
    }
    public static Stage returnStage() {
        return stage;
    }
    
    /**
     * @param stage the stage to set
     */
    public static void setStage(Stage stage) {
        Mainclass.stage = stage;
    }

        
}
