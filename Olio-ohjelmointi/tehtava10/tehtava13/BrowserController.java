/**
 * 
 */
package tehtava13;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;


import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;

/**
 * @author tkt
 *
 */

public class BrowserController implements Initializable {

    @FXML
    private Button exitButton;
    @FXML
    private Button backButton;
    @FXML
    private Button nextButton;
    @FXML
    private Button prevButton;
    @FXML
    private Button openButton;
    @FXML
    private Button loadButton;
    @FXML
    private Button refreshButton;
    @FXML
    private Button shotButton;
    @FXML
    private Button iniButton;
    @FXML
    private TextField addressBar;
    @FXML
    private WebView webview;
    @FXML
    private WebEngine engine;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        engine = webview.getEngine();
        webview.getEngine().load("http://www.google.fi");
    }

    public void loadSite() throws MalformedURLException{
        String url = addressBar.getText();
        if (url != null) {
            if (url.equals("index.html")) {
                File file = new File("index2.html");
                URL fileUrl = file.toURI().toURL();
                url = fileUrl.toString();
            }
            else if (!url.startsWith("http://")) {
                url = "http://" + url;
                addressBar.setText(url);
            }
            engine.load(url);
        }
    }

    public void EnterLoadSite(KeyEvent event) throws MalformedURLException {
        if (event.getCode() == KeyCode.ENTER) loadSite();
    }

    public void refreshSite() {
        engine.reload();
    }
    public void scriptIni() {
        if (engine != null) engine.executeScript("initialize()");
    }
    public void scriptShot() {
        if (engine != null) engine.executeScript("document.shoutOut()");
    }

    public void exit() {
        System.exit(0);
    }

}

