/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 02.06.17                                                              ****/
/**** Exercise: 5-5                                                               ****/
/*************************************************************************************/
package tehtava55;

abstract class WeaponBehavior {
    public String useWeapon() {
        return this.getClass().getSimpleName();
    }
}

public abstract class Character {
    public WeaponBehavior weapon;

    public Character() {
        // Character character = new Troll();
        // character.weapon = new Club();
    }
    public void fight() {
        System.out.println(this.getClass().getSimpleName() + " tappelee aseella " + weapon.useWeapon());
    }

}

class Queen extends Character {}
class King extends Character {}
class Knight extends Character {}
class Troll extends Character {}

class Sword extends WeaponBehavior {}
class Knife extends WeaponBehavior {}
class Axe extends WeaponBehavior {}
class Club extends WeaponBehavior {}
