/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 02.06.102.06.17                                                       ****/
/**** Exercise: 5-5                                                               ****/
/*************************************************************************************/
package tehtava55;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Mainclass {
    private static Character character; 
    public static void main (String[] args) {
        Scanner input = null;
        while(true) {
            System.out.println("*** TAISTELUSIMULAATTORI ***");
            System.out.println("1) Luo hahmo");
            System.out.println("2) Taistele hahmolla");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            input = new Scanner(new BufferedReader(new InputStreamReader(System.in)));
            try { 
                int choice = input.nextInt();
                switch (choice) {
                    case 0:
                        input.close();
                        return;
                    case 1:
                        System.out.println("Valitse hahmosi: ");
                        System.out.println("1) Kuningas");
                        System.out.println("2) Ritari");
                        System.out.println("3) Kuningatar");
                        System.out.println("4) Peikko");
                        System.out.print("Valintasi: ");
                        try {
                            int char_choice = input.nextInt();
                            switch (char_choice) {
                                case 1:
                                    character = new King();
                                    break;
                                case 2:
                                    character = new Knight();
                                    break;
                                case 3:
                                    character = new Queen();
                                    break;
                                case 4:
                                    character = new Troll();
                                    break;
                                default:
                                    System.out.println("Väärä valinta!");
                            } 
                        } catch (InputMismatchException e) {
                            System.out.println("Valinta ei ollut kokonaisluku!");
                        }
                        System.out.println("Valitse aseesi: ");
                        System.out.println("1) Veitsi");
                        System.out.println("2) Kirves");
                        System.out.println("3) Miekka");
                        System.out.println("4) Nuija");
                        System.out.print("Valintasi: ");
                        try { 
                            int wep_choice = input.nextInt();
                            switch (wep_choice) {
                                case 1:
                                    character.weapon = new Knife(); 
                                    break;
                                case 2:
                                    character.weapon = new Axe(); 
                                    break;
                                case 3:
                                    character.weapon = new Sword();
                                    break;
                                case 4:
                                    character.weapon = new Club(); 
                                    break;
                                default:
                                    System.out.println("Väärä valinta!");
                            }
                        } catch (InputMismatchException ex) {
                            System.out.println("Valinta ei ollut kokonaisluku!");
                        }
                        break;
                    case 2:
                        if (character != null && character.weapon != null) {
                            character.fight();
                        } else
                            System.out.println("Et ole valinnut vielä hahmoa!");
                        break;
                    default:
                        System.out.println("Väärä valinta!");
                }
            } catch(InputMismatchException e) {
                System.out.println("Valinta ei ollut kokonaisluku!");
            }
        }
    }
}

