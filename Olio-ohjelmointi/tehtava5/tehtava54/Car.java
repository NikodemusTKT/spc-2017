/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 01.06.17                                                              ****/
/**** Exercise: 5-4                                                               ****/
/*************************************************************************************/
package tehtava54;

import java.util.ArrayList;

class Body {
    public Body() {
        System.out.println("Valmistetaan: " + this.getClass().getSimpleName());
    }
}
class Chassis {
    public Chassis() {
        System.out.println("Valmistetaan: " + this.getClass().getSimpleName());
    }
}
class Wheel {
    public Wheel() {
        System.out.println("Valmistetaan: " + this.getClass().getSimpleName());
    }
}
class Engine {
    public Engine() {
        System.out.println("Valmistetaan: " + this.getClass().getSimpleName());
    }
}
public class Car {
    private Body body;
    private Chassis chassis;
    private Engine engine;
    private ArrayList<Wheel> wheels;
    private int amount_of_wheels = 4;
    public Car() {
        body = new Body();
        chassis = new Chassis();
        engine = new Engine();
        wheels = new ArrayList<Wheel>();
        for (int i = 0; i < amount_of_wheels; i++) {
            wheels.add(new Wheel());
        }
    }
    public void print() {
         System.out.println("Autoon kuuluu:");
         System.out.println("\t" + body.getClass().getSimpleName());
         System.out.println("\t" + chassis.getClass().getSimpleName());
         System.out.println("\t" + engine.getClass().getSimpleName());
         System.out.println("\t" + amount_of_wheels + " " +  wheels.get(0).getClass().getSimpleName());
    }
}
