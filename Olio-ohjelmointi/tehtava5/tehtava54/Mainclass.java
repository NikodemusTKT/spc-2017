/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 01.06.17                                                              ****/
/**** Exercise: 5-4                                                               ****/
/*************************************************************************************/
package tehtava54;

public class Mainclass {
    public static void main (String[] args) {
        Car c = new Car();
        c.print();
    }
}
