// Creator: Teemu Tanninen
// Student id: 0508505
// Date: 31.05.17
package tehtava34;

import java.util.ArrayList;

public class BottleDispenser  {
    private int bottles;
    private double money;
    private ArrayList<Bottle> bottle_array;

    public BottleDispenser() {
        bottles = 5;
        money = 0;
        String bb, bm; 
        double be, bs, bp;
        bottle_array = new ArrayList<Bottle>();
        for (int i = 0; i <= bottles; i++) {
            bottle_array.add(i, new Bottle());
        }
    }
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }

    public void buyBottle() {
        Bottle b = bottle_array.get(bottles -1);
        if (bottles > 0) {
            if (money <= 0 || b.getPrize() > money) {
                System.out.println("Syötä rahaa ensin!");
            } else {
                bottles -= 1;
                money -= 1;
                System.out.println("KACHUNK! " + b.getBrand() + " tipahti masiinasta!");
                removeBottle(b);
            }
        } 
        else {
            System.out.println("Pullot ovat loppu!");
        }
    }

    public void returnMoney() {
        money = 0;
        System.out.println("Klink klink. Sinne menivät rahat!");
    }
    public void removeBottle(Bottle b) {
        bottle_array.remove(b);
    }
    public void printBottles() {
        for (int i = 0; i <= bottles; i++) {
            System.out.println(i+1 + ". Nimi: " + bottle_array.get(i).getBrand()); 
            System.out.print("\tKoko: " + bottle_array.get(i).getSize()); 
            System.out.println("\tHinta: " + bottle_array.get(i).getPrize()); 
        }
    }
}
 


