// Creator: Teemu Tanninen
// Student id: 0508505
// Date: 31.05.17

package tehtava34;


public class Bottle {
    private String BottleBrand; 
    private String BottleManufacturer; 
    private double BottleEnergy; 
    private double BottleSize; 
    private double BottlePrize; 

    public Bottle() {
        BottleBrand = "Pepsi Max";
        BottleManufacturer = "Pepsi";
        BottleEnergy = 0.3;
        BottleSize = 0.5;
        BottlePrize = 1.80;
    }
    public Bottle(String bb, String bm, double be, double bs, double bp) {
        BottleBrand = bb;
        BottleManufacturer = bm;
        BottleEnergy = be;
        BottleSize = bs;
        BottlePrize = bp;
    }

    public String getBrand() {
        return BottleBrand;
    }
    public String getManufacturer(){
         return BottleManufacturer;
     }
     public double getEnergy(){
        return BottleEnergy;
    }
    public double getPrize(){
        return BottlePrize;
    }
    public double getSize(){
        return BottleSize;
    }

}
