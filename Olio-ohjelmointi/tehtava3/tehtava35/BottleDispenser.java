// Creator: Teemu Tanninen
// Student id: 0508505
// Date: 31.05.17

import java.util.ArrayList;

public class BottleDispenser  {
    static private int bottles;
    private double money;
    private ArrayList<Bottle> bottle_array;

    public BottleDispenser() {
        bottles = 5;
        money = 0;
        String bb, bm; 
        double be, bs, bp;
        bottle_array = new ArrayList<Bottle>();
        for (int i = 0; i<=bottles; i++) {
            if (i < 2) {
                bb = "Pepsi Max";
                bm = "Pepsi";
                be = 0.3;
                if (i < 1) {
                    bs = 0.5;
                    bp = 1.8;
                } else {
                    bs = 1.5;
                    bp = 2.2;
                }
            }
                else if (i < 4) {
                    bb = "Coca-Cola Zero";
                    bm = "Coca-Cola";
                    be = 0.4;
                    if (i < 3) {
                        bs = 0.5;
                        bp = 2.0;
                    } else {
                        bs = 1.5;
                        bp = 2.5;
                    }
                }
            else {
                bb = "Fanta Zero";
                bm = "Fanta";
                be = 0.5;
                bs = 0.5;
                bp = 1.95;
            }
            bottle_array.add(i, new Bottle(bb,bm,be,bs,bp));
        }
    }
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }

    public void buyBottle(int b_index) {
        Bottle b = bottle_array.get(b_index -1);
        if (bottles > 0) {
            if (money <= 0 || b.getPrize() > money) {
                System.out.println("Syötä rahaa ensin!");
            } else {
                bottles -= 1;
                money -= b.getPrize();
                System.out.println("KACHUNK! " + b.getBrand() + " tipahti masiinasta!");
                removeBottle(b);
            }
        } 
        else {
            System.out.println("Pullot ovat loppu!");
        }
    }

    public void returnMoney() {
        System.out.format("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€\n", money);
    }
    public void removeBottle(Bottle b) {
        bottle_array.remove(b);
    }
    public void printBottles() {
        for (int i = 0; i <= bottles; i++) {
            System.out.println(i+1 + ". Nimi: " + bottle_array.get(i).getBrand()); 
            System.out.print("\tKoko: " + bottle_array.get(i).getSize()); 
            System.out.println("\tHinta: " + bottle_array.get(i).getPrize()); 
        }
    }
}
 


