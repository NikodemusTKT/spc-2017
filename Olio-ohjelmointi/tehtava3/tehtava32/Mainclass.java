// Creator: Teemu Tanninen
// Student id: 0508505
// Date: 31.05.17
package tehtava32;

public class Mainclass {
    public static void main (String[] args) {
        BottleDispenser bt = new BottleDispenser();
        bt.addMoney();
        bt.buyBottle();
        bt.buyBottle();
        bt.addMoney();
        bt.addMoney();
        bt.buyBottle();
        bt.returnMoney();
    }
}
