// Creator: Teemu Tanninen
// Student id: 0508505
// Date: 31.05.17

package tehtava32;


public class Bottle {
    private String BottleBrand; 
    private String BottleManufacturer; 
    private double BottleEnergy; 

    public Bottle() {
        BottleBrand = "Pepsi Max";
        BottleManufacturer = "Pepsi";
        BottleEnergy = 0.3;
    }
    public Bottle(String bb, String bm, double be) {
        BottleBrand = bb;
        BottleManufacturer = bm;
        BottleEnergy = be;
    }
    public String getBrand() {
        return BottleBrand;
    }
    public String getManufacturer(){
         return BottleManufacturer;
     }
     public double getEnergy(){
        return BottleEnergy;
    }

}
