// Creator: Teemu Tanninen
// Student id: 0508505
// Date: 31.05.17
package tehtava33;

public class BottleDispenser  {
    private int bottles;
    private int money;
    private Bottle[] bottle_array;

    public BottleDispenser() {
        bottles = 5;
        money = 0;
        bottle_array = new Bottle[bottles];
        for (int i = 0; i<bottles; i++) {
            bottle_array[i] = new Bottle();
        }
    }
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }

    public void buyBottle() {
        Bottle b = bottle_array[bottles-1];
        if (bottles > 0) {
            if (money <= 0 || b.getPrize() > money) {
                System.out.println("Syötä rahaa ensin!");
            } else {
                bottles -= 1;
                money -= 1;
                System.out.println("KACHUNK! " + b.getBrand() + " tipahti masiinasta!");
            }
        } 
        else {
            System.out.println("Pullot ovat loppu!");
        }
    }

    public void returnMoney() {
        money = 0;
        System.out.println("Klink klink. Sinne menivät rahat!");
    }
}
 


