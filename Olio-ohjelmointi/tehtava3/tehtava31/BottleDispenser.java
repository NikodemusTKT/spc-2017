// Creator: Teemu Tanninen
// Student id: 0508505
// Date: 31.05.17

import java.util.ArrayList; 
package tehtava32;

public class BottleDispenser  {
    private int bottles;
    private int money;

    public BottleDispenser() {
        bottles = 5;
        money = 0;
    }
    public void addMoney() {
        money += 1;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }

    public void buyBottle() {
        if (bottles > 0) {
            if (money <= 0) {
                System.out.println("Syötä rahaa ensin!");
            } else {
                bottles -= 1;
                money -= 1;
                System.out.println("KACHUNK! Pullo tipahti masiinasta!");
            }
        } 
        else {
            System.out.println("Pullot ovat loppu!");
        }
    }

    public void returnMoney() {
        money = 0;
        System.out.println("Klink klink. Sinne menivät rahat!");
    }
}
 


