package tehtava31;

public class Mainclass {
    public static void main (String[] args) {
        BottleDispenser bt = new BottleDispenser();
        bt.addMoney();
        bt.buyBottle();
        bt.buyBottle();
        bt.addMoney();
        bt.addMoney();
        bt.buyBottle();
        bt.returnMoney();
    }
}
