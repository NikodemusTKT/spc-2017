/**
 * 
 */
package tehtava93;

import java.text.ParseException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * @author tkt
 *
 */
public class MoviesController {
    // Id:s for fxml controls
    @FXML
    // Comboboxes
    private ComboBox<String> theaterChoice;
    @FXML
    // Buttons
    private Button listButton, searchButton;
    @FXML
    private TextField showDay;
    @FXML
    private TextField startDay;
    @FXML
    private TextField endDay;
    @FXML
    private TextField nameSearch;
    // Textfields
    @FXML
    // Textareas
    private TextArea outputArea;
    @FXML
    private ListView<String> list;
    @FXML
    TheatreParser th;
    String errortext = "";

    /**
     *
     *
     *
     * @throws ParseException
     */
    @FXML
    public void initialize() throws ParseException {
        th = new TheatreParser();
        th.pickTheatres();
        theaterChoice.setValue(null);
        showDay.setText("");
        theaterChoice.setPromptText(th.theatres.get(0).getName());
        ObservableList<String> data = FXCollections.observableArrayList(th.getStringList());
        theaterChoice.setItems(data);
    }

    /**
     *
     *
     *
     * @throws ParseException
     */
    public void getDate() throws ParseException {
        TheatreParser sh = new TheatreParser();
        String id = theaterChoice.getValue();
        String day = showDay.getText();
        String startTime = startDay.getText();
        String endTime = endDay.getText();
        try {
            id = id.replaceAll("[\\D]","");
            day = DateValidator.Formatter(day,"dd.MM.yyyy","date");
            if (!id.isEmpty() && !day.isEmpty()) {
                sh.pickShows(id,day);
                if (startTime.isEmpty() && endTime.isEmpty()) {
                    ObservableList<String> shows = FXCollections.observableArrayList(sh.getShowStringList("",""));
                    list.setItems(shows);
                } else {
                    try 
                    {
                        ObservableList<String> shows = FXCollections.observableArrayList(sh.getShowStringList(startTime,endTime));
                        list.setItems(shows);
                        endDay.setText("");
                        startDay.setText("");
                    } catch (NullPointerException ex) {
                        errortext = "Syötetyt ajat eivät olleet oikein formatoituja.";
                        Alert loadAlert = new Alert(AlertType.ERROR);
                        loadAlert.setHeaderText(errortext);
                        loadAlert.setTitle("Syötevirhe");
                        loadAlert.showAndWait();
                    }
                }
            }
        } catch (NullPointerException e) {
            if (id == null && day == null) {
                errortext = "Et ole valinnut teatteria etkä syöttänyt päivämäärää.";
            }
            else if (id == null && day != null) {
                errortext = "Et ole valinnut teatteria.";
            }
            else {
                errortext = "Et ole syöttänyt päivämäärää tai se on väärin kirjoitettu.";
            }
            Alert loadAlert = new Alert(AlertType.ERROR);
            loadAlert.setHeaderText(errortext);
            loadAlert.setTitle("Syötevirhe");
            loadAlert.showAndWait();
        }

        
    }

    public void enterDate(KeyEvent ke) throws ParseException {
        if (ke.getCode().equals(KeyCode.ENTER)) {
            getDate();
        }
    }
}
