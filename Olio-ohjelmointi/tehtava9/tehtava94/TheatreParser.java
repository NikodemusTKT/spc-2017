package tehtava94;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class TheatreParser {
    private static final String theatreUrl = "http://www.finnkino.fi/xml/TheatreAreas/";
    private static final String showUrl = "http://www.finnkino.fi/xml/Schedule/?area=<teatterinID>&dt=<päivämäärä pp.kk.vvvv>"; 
    private final String dateFormat = "dd.MM.yyyy";
    private final String timeFormat = "HH:mm";
    public ArrayList<Theatre> theatres;
    public ArrayList<Shows> shows;
    Document doc;

    /**
     *
     */
    public TheatreParser() throws ParseException {
        theatres = new ArrayList<Theatre>();
        shows = new ArrayList<Shows>();

    }

    /**
     *
     *
     *
     * @throws ParseException
     */
    public void pickTheatres() throws ParseException {
        parseXml(theatreUrl,"","");
        parseTheatres();
    }

    /**
     *
     *
     * @param id
     * @param day
     *
     * @throws ParseException
     */
    public void pickShows(String id, String day) throws ParseException {
        parseXml(showUrl,id,day);
        parseShows();
    }


    /**
     *
     *
     * @param url
     * @param id
     * @param pvm
     */
    public void parseXml(String url, String id, String pvm) {
        if (url.equals(showUrl)) {
            url = url.replace("<teatterinID>",id).replace("<päivämäärä pp.kk.vvvv>", pvm);
            System.out.println(url);
        }
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(new URL(url).openStream());
            doc.getDocumentElement().normalize();

        }catch(ParserConfigurationException pce) {
            pce.printStackTrace();
        }catch(SAXException se) {
            se.printStackTrace();
        }catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     *
     *
     */
    public void parseTheatres() {
        Element docElement = doc.getDocumentElement();
        NodeList node = docElement.getElementsByTagName("TheatreArea");
        if (node != null && node.getLength() > 0) {
            for (int i=0; i < node.getLength(); i++) {
                Element el = (Element)node.item(i);
                Theatre object = getTheatre(el);
                theatres.add(object);
            }
        }
    }


    /**
     *
     *
     * @param el
     * @return
     */
    private Theatre getTheatre(Element el) {
        String name = getTextValue(el, "Name");
        int id = getIntValue(el, "ID");
        Theatre theatre = new Theatre(name,id);
        return theatre;
    }

    public void parseShows() throws ParseException {
        Element docElement = doc.getDocumentElement();
        NodeList node = docElement.getElementsByTagName("Show");
        if (node != null && node.getLength() > 0) {
            for (int i=0; i < node.getLength(); i++) {
                Element el = (Element)node.item(i);
                Shows show = getShow(el);
                shows.add(show);
            }
        }
    }

    /**
     *
     *
     * @param el
     * @return
     *
     * @throws ParseException
     */
    private Shows getShow(Element el) throws ParseException {
        int id = getIntValue(el, "ID");
        String Accounting = getTextValue(el, "dtAccounting");
        String[] ShowStart = getTextValue(el, "dttmShowStart").split("T");
        String ShowStartDay = DateValidator.Formatter(ShowStart[0],dateFormat,"date");
        String ShowStartClock = DateValidator.Formatter(ShowStart[1],timeFormat,"time");
        String[] ShowEnd = getTextValue(el, "dttmShowEnd").split("T");
        String ShowEndDay = DateValidator.Formatter(ShowEnd[0],dateFormat,"date");
        String ShowEndClock = DateValidator.Formatter(ShowEnd[1],timeFormat,"time");
        System.out.println(ShowEndClock);
        String Title = getTextValue(el, "Title");
        String OrginalTitle = getTextValue(el, "OriginalTitle");
        int ProductionYear = getIntValue(el, "ProductionYear");
        int Lenght = getIntValue(el, "LengthInMinutes");
        String Genre = getTextValue(el, "Genres");
        String ReleaseDate = getTextValue(el, "dtLocalRelease");
        int TheatreId = getIntValue(el, "TheatreID");
        int TheatreAuditriumId = getIntValue(el, "TheatreAuditriumID");
        String TheatreAndAuditorium = getTextValue(el, "TheatreAuditorium");
        // String language = getTextValue(el, "PresentationMethodAndLanguage");
        Shows show = new Shows(id, Accounting, ShowStartDay,ShowStartClock, ShowEndDay, ShowEndClock, Title, OrginalTitle, ProductionYear, Lenght, Genre, ReleaseDate, TheatreId, TheatreAuditriumId, TheatreAndAuditorium, "");

        return show ;
    }

    /**
     *
     *
     * @param element
     * @param tag
     * @return textValue from the Element
     */
    private String getTextValue(Element element, String tag) {
        // System.out.println("getTextValue initilized");
        String S_value = null;
        NodeList node = element.getElementsByTagName(tag);
        if (node != null && node.getLength() > 0) {
            Element el = (Element)node.item(0);
            S_value = el.getFirstChild().getNodeValue();
        }
        return S_value;
    }


    /**
     *
     *
     * @param el
     * @param tag
     * @return Integer value from the element
     */
    private int getIntValue(Element el, String tag) {
        // System.out.println("getIntValue initilized");
        return Integer.parseInt(getTextValue(el,tag));
    }

    public ArrayList<String> getStringList() {
        ArrayList<String> StringList = new ArrayList<String>();
        for (Theatre theatre: theatres) {
            StringBuilder sb = new StringBuilder();
            if (theatre.getName().matches("^Valitse.*")) continue;
            sb.append(String.format("%s %d", theatre.getName(),theatre.getId()));
            StringList.add(sb.toString());
        }
        return StringList;
    }
    // 1
    public ArrayList<String> getShowStringList(String startTime, String endTime) throws ParseException {
        ArrayList<String> StringList = new ArrayList<String>();
        for (Shows show: shows) {
            if (!startTime.isEmpty() && !endTime.isEmpty()) {
                if (!DateValidator.isTimeBetween(startTime,endTime,show.getShowStartClock())) continue;
            }
            // Case when startTime is given but endTime isn't.
            else if (!startTime.isEmpty() && endTime.isEmpty()) {
                if (!DateValidator.isAfter(startTime,show.getShowStartClock())) continue;
            }
            // Case when startTime is not given but endTime is given.
            else if (startTime.isEmpty() && !endTime.isEmpty()) {
                if (!DateValidator.isBefore(endTime,show.getShowStartClock())) continue;
            }
            StringList.add(show.stringBuilder());
        }
        return StringList;
    }

    /**
     * @return the showurl
     */
    public static String getShowurl() {
        return showUrl;
    }
}




