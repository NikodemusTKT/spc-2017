/**
 * 
 */
package tehtava92;

/**
 * @author tkt
 *
 */

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.fxml.FXMLLoader;


public class Mainclass extends Application{
	@Override
	public void start(Stage primaryStage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("Movies.fxml"));
			Scene scene = new Scene(root);
			scene.getStylesheets().add(getClass().getResource("Movies.css").toExternalForm());
            primaryStage.setTitle("Finnkinon eluokuvat");
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
		
}
