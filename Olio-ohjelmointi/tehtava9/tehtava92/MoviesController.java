/**
 * 
 */
package tehtava92;

import java.util.ArrayList;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 * @author tkt
 *
 */
public class MoviesController {
    // Id:s for fxml controls
    @FXML
    // Comboboxes
    private ComboBox<String> theaterChoice;
    @FXML
    // Buttons
    private Button listButton, searchButton;
    @FXML
    // Textfields
    private TextField showDay, startDay, endDay, nameSearch;
    @FXML
    // Textareas
    private TextArea outputArea;

    @FXML
    public void initialize() {
        TheatreParser th = new TheatreParser();
        // th.printStringLista(th.getStringList());
        theaterChoice.setPromptText(th.theatres.get(0).getName());
        ObservableList<String> data = FXCollections.observableArrayList(th.getStringList());
        theaterChoice.setItems(data);

    }

}
