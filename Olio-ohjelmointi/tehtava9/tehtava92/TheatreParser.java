package tehtava92;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class TheatreParser {
    private static final String theatreUrl = "http://www.finnkino.fi/xml/TheatreAreas/";
    public ArrayList<Theatre> theatres;
    Document doc;

    /**
     *
     */
    public TheatreParser() {
        theatres = new ArrayList<Theatre>();
        parseXml();
        parseDocument();
    }

    public void parseXml() {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(new URL(theatreUrl).openStream());

        }catch(ParserConfigurationException pce) {
            pce.printStackTrace();
        }catch(SAXException se) {
            se.printStackTrace();
        }catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    public void parseDocument() {
        // System.out.println("parseDocument initiliazed");
        Element docElement = doc.getDocumentElement();
        NodeList node = docElement.getElementsByTagName("TheatreArea");
        if (node != null && node.getLength() > 0) {
            for (int i=0; i < node.getLength(); i++) {
                Element el = (Element)node.item(i);
                Theatre theatre = getTheatre(el);
                theatres.add(theatre);
                // System.out.println("testi");
            }
        }
    }


    private Theatre getTheatre(Element el) {
        // System.out.println("getTheatre initilized");
        String name = getTextValue(el, "Name");
        int id = getIntValue(el, "ID");
        Theatre theatre = new Theatre(name,id);
        return theatre;
    }

    public String getTextValue(Element element, String tag) {
        // System.out.println("getTextValue initilized");
        String S_value = null;
        NodeList node = element.getElementsByTagName(tag);
        if (node != null && node.getLength() > 0) {
            Element el = (Element)node.item(0);
            S_value = el.getFirstChild().getNodeValue();
        }
        return S_value;
    }


    private int getIntValue(Element el, String tag) {
        // System.out.println("getIntValue initilized");
        return Integer.parseInt(getTextValue(el,tag));
    }

    public ArrayList<String> getStringList() {
        ArrayList<String> StringList = new ArrayList<String>();
        for (Theatre theatre: theatres) {
            StringBuilder sb = new StringBuilder();
            if (theatre.getName().matches("^Valitse.*")) continue;
            sb.append(String.format("%s %d", theatre.getName(),theatre.getId()));
            StringList.add(sb.toString());
        }
        return StringList;
    }
}




