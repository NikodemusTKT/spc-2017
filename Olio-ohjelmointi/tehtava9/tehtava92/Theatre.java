package tehtava92;

public class Theatre {
    private String name;
    private int id;

    /**
     *
     */
    public Theatre() {
    }

    /**
     * @param name
     * @param id
     */
    public Theatre(String name, int id) {
        this.name = name;
        this.id = id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

}

