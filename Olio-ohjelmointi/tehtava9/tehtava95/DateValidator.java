/**
 * 
 */
package tehtava95;

import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import jdk.nashorn.internal.runtime.ParserException;

/**
 * @author tkt
 *
 */
public final class DateValidator {
	final static List<String> ValidFormatList = Arrays.asList( "yyyy-MM-dd", "yyyy.MM.dd", "yyyy/MM/dd", "dd/MM/yyyy", "dd-MM-yyyy", "dd.MM.yyyy", "MM.dd.yyyy", "MM-dd-yyyy", "MM/dd/yyyy", "ddMMyyyy", "ddMyyyy");
    final static List<String> ValidTimeFormatList = Arrays.asList("HH:mm:ss", "HH.mm.ss","HH:mm", "HH.mm", "HH"); 

    public static Date parseDate(String date, List<String> formats) {
        SimpleDateFormat sdf = new SimpleDateFormat();
        sdf.setLenient(false);
        ParsePosition position = new ParsePosition(0);
        Date day = null;

        for (int i = 0; i < formats.size(); i++) {
            sdf.applyPattern(formats.get(i));
            position.setIndex(0);
            position.setErrorIndex(-1);
            day = sdf.parse(date, position);
            if (position.getErrorIndex() == -1) {
                return day;
            }
        }
        return day;
    }

    public static String Formatter(String date, String format, String type) {
        List<String> FormatList = null;
        String returnDate = null;
        Date day = null;
        if (type.equalsIgnoreCase("date")) 
            FormatList = ValidFormatList;
        else if (type.equalsIgnoreCase("time"))
        {
            if (date.equals("24")) date = "00";
            FormatList = ValidTimeFormatList;
        }
        try {
            day = parseDate(date,FormatList);
            SimpleDateFormat sdf = new SimpleDateFormat(format);
            if (day != null) {
                returnDate = sdf.format(day);
                return returnDate;
            }
        } catch (ParserException ex) {
            ex.printStackTrace();
        }
        return returnDate;
    }

    public static boolean isTimeBetween(String startTime,String endTime, String compareTime) {
        startTime = Formatter(startTime,"HH:mm:ss","time");
        endTime = Formatter(endTime,"HH:mm:ss","time");
        compareTime = Formatter(compareTime,"HH:mm:ss","time");
        Date sTime = dateFromHourMinSec(startTime);
        Date eTime = dateFromHourMinSec(endTime);
        Date cTime = dateFromHourMinSec(compareTime);
        return (cTime.after(sTime) || cTime.compareTo(sTime) == 0) && cTime.before(eTime);

    }
    public static boolean isAfter(String startTime, String compareTime) {
        startTime = Formatter(startTime,"HH:mm:ss","time");
        compareTime = Formatter(compareTime,"HH:mm:ss","time");
        Date sTime = dateFromHourMinSec(startTime);
        Date cTime = dateFromHourMinSec(compareTime);
        return cTime.after(sTime) || cTime.compareTo(sTime) == 0;

    }
    public static boolean isBefore(String endTime, String compareTime) {
        endTime = Formatter(endTime,"HH:mm:ss","time");
        compareTime = Formatter(compareTime,"HH:mm:ss","time");
        Date eTime = dateFromHourMinSec(endTime);
        Date cTime = dateFromHourMinSec(compareTime);
        return cTime.before(eTime) || cTime.compareTo(eTime) == 0;

    }
    private static Date dateFromHourMinSec(final String hhmmss)
    {
        final String[] hms = hhmmss.split(":");
        final GregorianCalendar gc = new GregorianCalendar();
        gc.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hms[0]));
        gc.set(Calendar.MINUTE, Integer.parseInt(hms[1]));
        gc.set(Calendar.SECOND, Integer.parseInt(hms[2]));
        gc.set(Calendar.MILLISECOND, 0);
        return gc.getTime();
    }
}





