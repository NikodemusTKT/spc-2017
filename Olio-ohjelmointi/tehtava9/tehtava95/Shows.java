/**
 * 
 */
package tehtava95;

import java.text.ParseException;
import java.util.Comparator;
import java.util.Date;


/**
 * @author tkt
 *
 */
public class Shows implements Comparable<Shows> {
    private final String showTemplate = "%s (%d) \t Esitysaika: %s - %s \t Pituus: %d min \t Sali: %s \t Genre: %s";
    private final String scheduleTemplate = "%-20s \t %-60s \t %-10s \t %s - %s \t Kesto: %d min \t Genre: %s"; 

    private int id;
    private String Accounting;
    private String ShowStart;
    private String ShowStartClock;
    private String ShowEnd;
    private String ShowEndCloc;
    private String Title;
    private String OrginalTitle;
    private int ProductionYear;
    private int Lenght;
    private String Genre;
    private String ReleaseDate;
    private String Theatre;
    private int TheatreId;
    private int TheatreAuditriumId;
    private String TheatreAndAuditorium;
    private String Auditorium;

    /**
     *
     */
    public Shows() {
    }


    /**
     * @param id
     * @param accounting
     * @param showStart
     * @param showStartClock
     * @param showEnd
     * @param showEndCloc
     * @param title
     * @param orginalTitle
     * @param productionYear
     * @param lenght
     * @param genre
     * @param releaseDate
     * @param theatre
     * @param theatreId
     * @param theatreAuditriumId
     * @param theatreAndAuditorium
     * @param auditorium
     */
    public Shows(int id, String accounting, String showStart, String showStartClock, String showEnd, String showEndCloc,
            String title, String orginalTitle, int productionYear, int lenght, String genre, String releaseDate,
            String theatre, int theatreId, int theatreAuditriumId, String theatreAndAuditorium, String auditorium) {
        this.id = id;
        Accounting = accounting;
        ShowStart = showStart;
        ShowStartClock = showStartClock;
        ShowEnd = showEnd;
        ShowEndCloc = showEndCloc;
        Title = title;
        OrginalTitle = orginalTitle;
        ProductionYear = productionYear;
        Lenght = lenght;
        Genre = genre;
        ReleaseDate = releaseDate;
        Theatre = theatre;
        TheatreId = theatreId;
        TheatreAuditriumId = theatreAuditriumId;
        TheatreAndAuditorium = theatreAndAuditorium;
        Auditorium = auditorium;
    }

    public int compareTo(Shows s1) {return 1;}


    public static Comparator<Shows> TitleComparator = new Comparator<Shows>() {
        @Override
        public int compare(Shows s1, Shows s2) {
            return (s1.getTitle().compareTo(s2.getTitle()));
        }
    };
    public static Comparator<Shows> TimeComparator = new Comparator<Shows>() {
        @Override
        public int compare(Shows s1, Shows s2) {
            Date time1 = DateValidator.parseDate(s1.getShowStartClock(),DateValidator.ValidTimeFormatList); 
            Date time2 = DateValidator.parseDate(s2.getShowStartClock(),DateValidator.ValidTimeFormatList); 
            return (time1.compareTo(time2));
        }
    };
    public static Comparator<Shows> ComparatorByCityAndTime = new Comparator<Shows>() {
        @Override
        public int compare(Shows s1, Shows s2) {
            Date time1 = DateValidator.parseDate(s1.getShowStartClock(),DateValidator.ValidTimeFormatList); 
            Date time2 = DateValidator.parseDate(s2.getShowStartClock(),DateValidator.ValidTimeFormatList); 
            int flag = s1.getTheatre().split(",")[1].compareTo(s2.getTheatre().split(",")[1]);
            if (flag==0) flag = s1.getTheatreAndAuditorium().compareTo(s2.getTheatreAndAuditorium());
            if (flag==0) flag = time1.compareTo(time2); 
            return flag;
        }
    };

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the accounting
     */
    public String getAccounting() {
        return Accounting;
    }

    /**
     * @param accounting the accounting to set
     */
    public void setAccounting(String accounting) {
        Accounting = accounting;
    }

    /**
     * @return the showStart
     */
    public String getShowStart() {
        return ShowStart;
    }

    /**
     * @param showStart the showStart to set
     */
    public void setShowStart(String showStart) {
        ShowStart = showStart;
    }

    /**
     * @return the showStartClock
     */
    public String getShowStartClock() {
        return ShowStartClock;
    }

    /**
     * @param showStartClock the showStartClock to set
     */
    public void setShowStartClock(String showStartClock) {
        ShowStartClock = showStartClock;
    }

    /**
     * @return the showEnd
     */
    public String getShowEnd() {
        return ShowEnd;
    }

    /**
     * @param showEnd the showEnd to set
     */
    public void setShowEnd(String showEnd) {
        ShowEnd = showEnd;
    }

    /**
     * @return the showEndCloc
     */
    public String getShowEndCloc() {
        return ShowEndCloc;
    }

    /**
     * @param showEndCloc the showEndCloc to set
     */
    public void setShowEndCloc(String showEndCloc) {
        ShowEndCloc = showEndCloc;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return Title;
    }

    /**
     * @param title the title to set
     */
    public void setTitle(String title) {
        Title = title;
    }

    /**
     * @return the orginalTitle
     */
    public String getOrginalTitle() {
        return OrginalTitle;
    }

    /**
     * @param orginalTitle the orginalTitle to set
     */
    public void setOrginalTitle(String orginalTitle) {
        OrginalTitle = orginalTitle;
    }

    /**
     * @return the productionYear
     */
    public int getProductionYear() {
        return ProductionYear;
    }

    /**
     * @param productionYear the productionYear to set
     */
    public void setProductionYear(int productionYear) {
        ProductionYear = productionYear;
    }

    /**
     * @return the lenght
     */
    public int getLenght() {
        return Lenght;
    }

    /**
     * @param lenght the lenght to set
     */
    public void setLenght(int lenght) {
        Lenght = lenght;
    }

    /**
     * @return the genre
     */
    public String getGenre() {
        return Genre;
    }

    /**
     * @param genre the genre to set
     */
    public void setGenre(String genre) {
        Genre = genre;
    }

    /**
     * @return the releaseDate
     */
    public String getReleaseDate() {
        return ReleaseDate;
    }

    /**
     * @param releaseDate the releaseDate to set
     */
    public void setReleaseDate(String releaseDate) {
        ReleaseDate = releaseDate;
    }

    /**
     * @return the theatre
     */
    public String getTheatre() {
        return Theatre;
    }

    /**
     * @param theatre the theatre to set
     */
    public void setTheatre(String theatre) {
        Theatre = theatre;
    }

    /**
     * @return the theatreId
     */
    public int getTheatreId() {
        return TheatreId;
    }

    /**
     * @param theatreId the theatreId to set
     */
    public void setTheatreId(int theatreId) {
        TheatreId = theatreId;
    }

    /**
     * @return the theatreAuditriumId
     */
    public int getTheatreAuditriumId() {
        return TheatreAuditriumId;
    }

    /**
     * @param theatreAuditriumId the theatreAuditriumId to set
     */
    public void setTheatreAuditriumId(int theatreAuditriumId) {
        TheatreAuditriumId = theatreAuditriumId;
    }

    /**
     * @return the theatreAndAuditorium
     */
    public String getTheatreAndAuditorium() {
        return TheatreAndAuditorium;
    }

    /**
     * @param theatreAndAuditorium the theatreAndAuditorium to set
     */
    public void setTheatreAndAuditorium(String theatreAndAuditorium) {
        TheatreAndAuditorium = theatreAndAuditorium;
    }

    /**
     * @return the auditorium
     */
    public String getAuditorium() {
        return Auditorium;
    }

    /**
     * @param auditorium the auditorium to set
     */
    public void setAuditorium(String auditorium) {
        Auditorium = auditorium;
    }

    public String stringBuilder() throws ParseException {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format(showTemplate, getTitle(),getProductionYear(),getShowStartClock(),getShowEndCloc(),getLenght(),getAuditorium(),getGenre()));
        return sb.toString();
    }

    public String scheduleBuilder() throws ParseException {
        StringBuilder sb = new StringBuilder();
        String output = String.format(scheduleTemplate, getTitle(), getTheatreAndAuditorium(), getShowStart(), getShowStartClock(), getShowEndCloc(), getLenght(), getGenre());
        sb.append(output);
        return sb.toString();
    }


    

}
