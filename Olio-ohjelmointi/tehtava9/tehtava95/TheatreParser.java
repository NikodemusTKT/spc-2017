package tehtava95;

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class TheatreParser {
    // needed urls for the xml parsers;
    private static final String theatreUrl = 
        "http://www.finnkino.fi/xml/TheatreAreas/";
    private static final String showUrl = 
        "http://www.finnkino.fi/xml/Schedule/?area=<teatterinID>&dt=<paivamaara pp.kk.vvvv>"; 
    private final String dateFormat = "dd.MM.yyyy";
    private final String timeFormat = "HH:mm";
    // public ArrayList<Theatre> theatres;
    public ArrayList<Shows> shows;
    private LinkedHashMap<String, Theatre> linkedTheatres;
    Document doc;


    /**
     *
     */
    private TheatreParser() {
        shows = new ArrayList<Shows>();
        this.linkedTheatres = new LinkedHashMap<>();
    }

    private static class Singleton {
            private static final TheatreParser INSTANCE = new TheatreParser();
        }

    /**
     * @return the instance
     */
    public static TheatreParser getInstance(){
        return Singleton.INSTANCE;
    }

    /**
     *
     *
     */
    public void pickTheatres() {
        parseXml(theatreUrl,"","");
        parseTheatres();
    }


    /**
     *
     *
     * @param id
     * @param day
     *
     */
    public void pickShows(String id, String day) {
        parseXml(showUrl,id,day);
        parseShows("");
        shows.sort(Shows.TitleComparator);
    }


    public void pickSchedules(String search, String pvm) {
    linkedTheatres.entrySet().parallelStream()
        .filter(e -> e.getKey() .matches("^(?!Valitse|Helsinki|Espoo|Vantaa).*"))
        .map(id -> id.getValue().getId())
        .forEach(id -> {
            parseXml(showUrl,id.toString(),pvm);
            parseShows(search);
        });
        shows.sort(Shows.ComparatorByCityAndTime);
        // for (Map.Entry<String, Theatre> entry: linkedTheatres.entrySet()) {
        //     String name = entry.getValue().getName();
        //     int id = entry.getValue().getId();
        //     // Skip these since they are are contained in paakaupunkiseutu xml
        //     if (name.matches("(Valitse|Helsinki|Espoo|Vantaa).*")) continue; 
        //     parseXml(showUrl,Integer.toString(id),pvm);
        //     parseShows(search);
        // }
    }

    /**
     *
     *
     * @param url
     * @param id
     * @param pvm
     */
    public void parseXml(String url, String id, String pvm) {
        if (url.equals(showUrl)) {
            url = url.replace("<teatterinID>",id);
            url = url.replace("<paivamaara pp.kk.vvvv>", pvm);
        }
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            doc = db.parse(new URL(url).openStream());
            doc.getDocumentElement().normalize();

        }catch(ParserConfigurationException pce) {
            pce.printStackTrace();
        }catch(SAXException se) {
            se.printStackTrace();
        }catch(IOException ioe) {
            ioe.printStackTrace();
        }
    }

    /**
     *
     *
     */
    public void parseTheatres() {
        Element docElement = doc.getDocumentElement();
        NodeList node = docElement.getElementsByTagName("TheatreArea");
        if (node != null && node.getLength() > 0) {
            for (int i=0; i < node.getLength(); i++) {
                Element el = (Element)node.item(i);
                Theatre object = getTheatre(el);
                if (object.getName().matches("^Valitse.*")) continue;
                linkedTheatres.put(object.getName(),object);
            }
        }
    }


    /**
     * This method creates new Theater instance from parsed data
     *
     * @param el
     * @return
     */
    private Theatre getTheatre(Element el) {
        String name = getTextValue(el, "Name");
        int id = getIntValue(el, "ID");
        Theatre theatre = new Theatre(name,id);
        return theatre;
    }

    public void parseShows(String searchTitle) {
        Element docElement = doc.getDocumentElement();
        NodeList node = docElement.getElementsByTagName("Show");
        if (node != null && node.getLength() > 0) {
            for (int i=0; i < node.getLength(); i++) {
                Element el = (Element)node.item(i);
                Shows show = getShow(el);
                if (!searchTitle.isEmpty())
                    if (!show.getTitle().matches("(?i)"+".*"+searchTitle+".*")) continue;
                shows.add(show);
            }
        }
    }

    /**
     * This method takes element and creates new Shows instance with parsed data.
     *
     * @param el
     * @return
     *
     */
    private Shows getShow(Element el) {
        int id = getIntValue(el, "ID");
        String Accounting = getTextValue(el, "dtAccounting");
        String[] ShowStart = getTextValue(el, "dttmShowStart").split("T");
        String ShowStartDay = DateValidator.Formatter(ShowStart[0],dateFormat,"date");
        String ShowStartClock = DateValidator.Formatter(ShowStart[1],timeFormat,"time");
        String[] ShowEnd = getTextValue(el, "dttmShowEnd").split("T");
        String ShowEndDay = DateValidator.Formatter(ShowEnd[0],dateFormat,"date");
        String ShowEndClock = DateValidator.Formatter(ShowEnd[1],timeFormat,"time");
        String Title = getTextValue(el, "Title");
        String OrginalTitle = getTextValue(el, "OriginalTitle");
        int ProductionYear = getIntValue(el, "ProductionYear");
        int Lenght = getIntValue(el, "LengthInMinutes");
        String Genre = getTextValue(el, "Genres");
        String ReleaseDate = getTextValue(el, "dtLocalRelease");
        String Theatre = getTextValue(el, "Theatre");
        int TheatreId = getIntValue(el, "TheatreID");
        int TheatreAuditriumId = getIntValue(el, "TheatreAuditriumID");
        String TheatreAndAuditorium = getTextValue(el, "TheatreAndAuditorium");
        String Auditorium = getTextValue(el, "TheatreAuditorium");
        // String language = getTextValue(el, "PresentationMethodAndLanguage");
        Shows show = new Shows(id, Accounting, ShowStartDay,ShowStartClock, ShowEndDay, ShowEndClock, Title, OrginalTitle, ProductionYear, Lenght, Genre, ReleaseDate, Theatre, TheatreId, TheatreAuditriumId, TheatreAndAuditorium, Auditorium);

        return show ;
    }

    /**
     *
     *
     * @param element
     * @param tag
     * @return textValue from the Element
     */
    private String getTextValue(Element element, String tag) {
        String S_value = null;
        NodeList node = element.getElementsByTagName(tag);
        if (node != null && node.getLength() > 0) {
            Element el = (Element)node.item(0);
            S_value = el.getFirstChild().getNodeValue();
        }
        return S_value;
    }

/**
     *
     *
     * @param el
     * @param tag
     * @return Integer value from the element
     */
    private int getIntValue(Element el, String tag) {
        return Integer.parseInt(getTextValue(el,tag));
    }

    public ArrayList<String> getStringList() {
        ArrayList<String> StringList = new ArrayList<String>();
        for (Map.Entry<String, Theatre> entry: linkedTheatres.entrySet()) {
            StringBuilder sb = new StringBuilder();
            String name = entry.getKey();
            int id = entry.getValue().getId();
            if (name.matches("^Valitse.*")) continue;
            System.out.println(name);
            sb.append(String.format("%s %d", name,id));
            StringList.add(sb.toString());
        }
        // StringList.sort(
        return StringList;
    }
    // 1
    /**
     * Method that builds String Array from shows in shows list
     *
     * @param startTime
     * @param endTime
     * @param type
     * @return
     *
     */
    public ArrayList<String> getShowStringList(String startTime, String endTime, String type) {
        ArrayList<String> StringList = new ArrayList<String>();
        for (Shows show: shows) {
            if (!startTime.isEmpty() && !endTime.isEmpty()) {
                if (!DateValidator.isTimeBetween(startTime,endTime,show.getShowStartClock())) continue;
            }
            // Case when startTime is given but endTime isn't.
            else if (!startTime.isEmpty() && endTime.isEmpty()) {
                if (!DateValidator.isAfter(startTime,show.getShowStartClock())) continue;
            }
            // Case when startTime is not given but endTime is given.
            else if (startTime.isEmpty() && !endTime.isEmpty()) {
                if (!DateValidator.isBefore(endTime,show.getShowStartClock())) continue;
            }
            // Build different kind of string lists depending on given type
            if (type.equalsIgnoreCase("shows"))
                try {
                    StringList.add(show.stringBuilder());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            else if (type.contentEquals("schedules"))
                try {
                    StringList.add(show.scheduleBuilder());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
        }
       // Empty the shows list after the String List has been populated 
        shows.clear();
        return StringList;
    }

    /**
     * @return the linkedTheatres
     */
    public LinkedHashMap<String, Theatre> getLinkedTheatres() {
        return linkedTheatres;
    }

}





