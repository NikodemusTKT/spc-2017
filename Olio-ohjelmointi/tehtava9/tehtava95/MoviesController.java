/**
 * 
 */
package tehtava95;

import java.text.ParseException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;

/**
 * @author tkt
 *
 */
public class MoviesController {
    // Id:s for fxml controls
    @FXML
    // Comboboxes
    private ComboBox<String> theaterChoice;
    @FXML
    // Buttons
    private Button listButton;
    @FXML
    private Button searchButton;
    @FXML
    private TextField showDay;
    @FXML
    private TextField startDay;
    @FXML
    private TextField endDay;
    @FXML
    private TextField nameSearch;
    // Textfields
    @FXML
    // Textareas
    private TextArea outputArea;
    @FXML
    private ListView<String> list;
    @FXML
    private TheatreParser th;
    private String errortext = "";

    /**
     *
     *
     *
     * @throws ParseException
     */
    @FXML
    public void initialize() throws ParseException {
        th = TheatreParser.getInstance();
        th.pickTheatres();
        theaterChoice.setValue(null);
        showDay.setText("");
        theaterChoice.setPromptText("Valitse alue/teatteri");
        ObservableMap<String,Theatre> data = FXCollections.observableMap((th.getLinkedTheatres()));
        theaterChoice.getItems().setAll(data.keySet());
        // Enter keyevents for input fields
        startDay.setOnKeyPressed(this::enterDate);
        endDay.setOnKeyPressed(this::enterDate);
        nameSearch.setOnKeyPressed(this::enterSearch);
    }
    /**
     *
     *
     *
     */
    public void getDate() {
        TheatreParser sh = TheatreParser.getInstance();
        String startTime = startDay.getText();
        String endTime = endDay.getText();
        String day=null,key=null,id = null;
        try {
            key = theaterChoice.getValue();
            id = Integer.toString(th.getLinkedTheatres().get(key).getId());
            day = showDay.getText();
            if (!day.isEmpty()) 
                day = DateValidator.Formatter(day,"dd.MM.yyyy","date");
            else day = "";
            if (!id.isEmpty()) {
                sh.pickShows(id,day);
                if (startTime.isEmpty() && endTime.isEmpty()) {
                    ObservableList<String> shows = FXCollections.observableArrayList(sh.getShowStringList("","","shows"));
                    list.setItems(shows);
                } else {
                    try 
                    {
                        ObservableList<String> shows = FXCollections.observableArrayList(sh.getShowStringList(startTime,endTime,"shows"));
                        list.setItems(shows);
                        endDay.setText("");
                        startDay.setText("");
                    } catch (NullPointerException ex) {
                        errortext = "Syötetyt ajat eivät olleet oikein formatoituja.";
                        Alert loadAlert = new Alert(AlertType.ERROR);
                        loadAlert.setHeaderText(errortext);
                        loadAlert.setTitle("Syötevirhe");
                        loadAlert.showAndWait();
                    }
                }
            }
        } catch (NullPointerException e) {
            if (id == null && day == null) {
                errortext = "Et ole valinnut teatteria etkä syöttänyt päivämäärää.";
            }
            else if (id == null && day != null) {
                errortext = "Et ole valinnut teatteria.";
            }
            else {
                errortext = "Et ole syöttänyt päivämäärää tai se on väärin kirjoitettu.";
            }
            Alert loadAlert = new Alert(AlertType.ERROR);
            loadAlert.setHeaderText(errortext);
            loadAlert.setTitle("Syötevirhe");
            loadAlert.showAndWait();
        }

        
    }


    public void  searchField() {
        String search = nameSearch.getText();
        String pvm = showDay.getText();
        ObservableList<String> shows;
        if (!pvm.isEmpty()) 
            pvm = DateValidator.Formatter(pvm,"dd.MM.yyyy","date");
        else pvm  = "";
        if (search != null) {
            th.pickSchedules(search, pvm);
            shows = FXCollections.observableArrayList(th.getShowStringList("", "", "schedules"));
            list.setItems(shows);
        }
    }
    public void enterDate(KeyEvent ke) {
        if (ke.getCode() == (KeyCode.ENTER))
            getDate();
        }
    public void enterSearch(KeyEvent ke) {
        if (ke.getCode() == (KeyCode.ENTER)) 
            searchField();
    }
}

