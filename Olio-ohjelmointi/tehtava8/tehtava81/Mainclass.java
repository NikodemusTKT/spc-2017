package tehtava81;
// Creator: Teemu Tanninen
// Student id: 0508505
// Date: 31.05.17


import java.util.Scanner;

public class Mainclass {
    public static void main (String[] args) {
        BottleDispenser bt = BottleDispenser.getInstance();
        Scanner input = null;
        while (true) {
            System.out.println("\n*** LIMSA-AUTOMAATTI ***");
            System.out.println("1) Lisää rahaa koneeseen");
            System.out.println("2) Osta pullo");
            System.out.println("3) Ota rahat ulos");
            System.out.println("4) Listaa koneessa olevat pullot");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            try {
                input = new Scanner (System.in);
                String in = input.nextLine();
                int valinta = Integer.parseInt(in);
                if (valinta == 0) {
                    input.close();
                    break;
                }
                else if (valinta == 1) {
                    bt.addMoney();
                }
                else if (valinta == 2) {
                    bt.printBottles();
                    System.out.print("Valintasi: ");
                    int b_choice = input.nextInt();
                    bt.buyBottle(b_choice);
                }
                else if (valinta == 3) {
                    bt.returnMoney();
                }
                else if (valinta == 4) {
                    bt.printBottles();
                }
                else {
                    System.out.println("Valinta ei kelpaa!");
                }
            } catch (NumberFormatException e) {
                System.out.println("Valinta ei ollut kokonaisluku!");
            } 
        }
    }
}
