package tehtava83;


import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;


public class MainController {
	@FXML
	private TextField BottleChoice;
	@FXML
	private TextArea textarea;
	@FXML
	private Button buyButton, moneyButton, returnButton, exitButton;
    @FXML
    private Label label, sliderValue;
    @FXML
    private Slider moneySlider;

	@FXML
    BottleDispenser bd = BottleDispenser.getInstance();

    @FXML
	private void initialize() {
        printBottles();
        sliderValue.setText("0.00 €");

	}

	@FXML
    private void BuyBottle() {
        String input = null;
        input = BottleChoice.getText();
        if (!input.isEmpty()) {
            try {
                int choice = Integer.parseInt(input);
                String text = bd.buyBottle(choice);
                printBottles();
                textarea.setText(textarea.getText() + text);
            } catch (NumberFormatException e) {
                textarea.setText(textarea.getText() + "Valinta ei ollut kokonaisluku!\n");
            }
        } else {
            printBottles();
        }
        BottleChoice.setText("");
    }

    @FXML
    private void addMoney() {
        String output = String.format("%s%s", textarea.getText(),bd.addMoney(moneySlider.getValue()));
        textarea.setText(output);
        moneySlider.setValue(0);
        sliderValue.setText("0.00 €");
    }
    @FXML
    private void returnMoney() {
        String output = String.format("%s%s", textarea.getText(),bd.returnMoney());
        textarea.setText(output);
    }
    @FXML
    private void printBottles() {
        String bottles = bd.printBottles();
        String output = String.format("Automaatissa olevat pullot:\n\n%sKirjoita valintasi syöttökenttään.\n", bottles);
        textarea.setText(output);
    }
    
    @FXML
    public void EnterKey(KeyEvent enter) {
        if (enter.getCode().equals(KeyCode.ENTER)) {
            BuyBottle();
        }
    }

    @FXML
    private void exit() {
        System.exit(0);
    }
    @FXML
    private void sliderChange() {
        sliderValue.setText(String.format("%.2f €", moneySlider.getValue()));
    }



}
