package tehtava83;
// Creator: Teemu Tanninen
// Student id: 0508505
// Date: 31.05.17

import java.util.ArrayList;

public class BottleDispenser  {
    private static BottleDispenser instance;
    private int bottles;
    private double money;
    private ArrayList<Bottle> bottle_array;

    public BottleDispenser() {
        bottles = 5;
        money = 0;
        String bb, bm; 
        double be, bs, bp;
        bottle_array = new ArrayList<Bottle>();
        for (int i = 0; i<=bottles; i++) {
            if (i < 2) {
                bb = "Pepsi Max";
                bm = "Pepsi";
                be = 0.3;
                if (i < 1) {
                    bs = 0.5;
                    bp = 1.8;
                } else {
                    bs = 1.5;
                    bp = 2.2;
                }
            }
                else if (i < 4) {
                    bb = "Coca-Cola Zero";
                    bm = "Coca-Cola";
                    be = 0.4;
                    if (i < 3) {
                        bs = 0.5;
                        bp = 2.0;
                    } else {
                        bs = 1.5;
                        bp = 2.5;
                    }
                }
            else {
                bb = "Fanta Zero";
                bm = "Fanta";
                be = 0.5;
                bs = 0.5;
                bp = 1.95;
            }
            bottle_array.add(i, new Bottle(bb,bm,be,bs,bp));
        }
    }
    public String addMoney(double m) {
        money += m;
        return("Klink! Lisää rahaa laitteeseen!\n");
    }

    public String buyBottle(int b_index) {
        if (b_index <= bottle_array.size()) {
            Bottle b = bottle_array.get(b_index -1);
            if (bottles > 0) {
                if (money <= 0 || b.getPrize() > money) {
                    return("Syötä rahaa ensin!\n");
                }
                else {
                    bottles -= 1;
                    money -= b.getPrize();
                    removeBottle(b);
                    return("KACHUNK! " + b.getBrand() + " tipahti masiinasta!\n");
                }
            }
            else {
                return("Pullot ovat loppu!\n");
            }
        } else {
            return("Pulloa ei löydy automaatista.");
        }
    }


    public String returnMoney() {
        String output = String.format("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€\n", money);
        money = 0;
        return output;
    }
    public void removeBottle(Bottle b) {
        bottle_array.remove(b);
    }
    public String printBottles() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i <= bottles; i++) {
            sb.append(i+1 + ". Nimi: " + bottle_array.get(i).getBrand() + "\n"); 
            sb.append("\tKoko: " + bottle_array.get(i).getSize() + "\n"); 
            sb.append("\tHinta: " + bottle_array.get(i).getPrize() + "\n"); 
        }
        return sb.toString();
    }

    public static BottleDispenser getInstance() {
        if (instance == null) {
            instance = new BottleDispenser();
        }
        return instance;
    }
}
 


