package tehtava85;


import java.io.IOException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;


public class MainController {
    
    ObservableList<String> bottles = FXCollections.observableArrayList("Battery","Coca-Cola Zero", "Coca Cola", "Fanta Zero", "Fanta", "Pepsi Max", "Pepsi", "Sprite");
    ObservableList<Double> sizes = FXCollections.observableArrayList(0.3, 0.5, 1.0, 1.5);
    
	@FXML
	private TextField BottleChoice;
	@FXML
	private TextArea textarea;
	@FXML
	private Button buyButton, moneyButton, returnButton, exitButton, showButton, saveButton;
    @FXML
    private Label label, sliderValue, machineMoney;
    @FXML
    private Slider moneySlider;
    @FXML
    private ComboBox<String> bottleChoice;
    @FXML
    private ComboBox<Double> sizeChoice;

	@FXML
    BottleDispenser bd = BottleDispenser.getInstance();

    @FXML
	private void initialize() {
        bd.addBottle(2, "Battery", "Oy Sinebrychoff Ab", 50.0, 0.3, 2.5); 
        bd.addBottle(2, "Sprite", "Coca-Cola", 43.0, 0.5, 2.5); 
        bottleChoice.setItems(bottles);
        sizeChoice.setItems(sizes);
        sliderValue.setText("0.00 €");
        textarea.setText("Valitse ostettava virvoike ja sen koko valikon pudotusvalikoista.\n");
        machineMoney.setText(String.format("Koneessa: %.2f€", bd.getMoney()));
    
	}

	@FXML
    private void BuyBottle() {
        String brand = bottleChoice.getValue();
        Double size = sizeChoice.getValue();
        try {
            String text = bd.buyBottle(brand,size);
            textarea.setText(text);
            bottleChoice.setValue(null);
            sizeChoice.setValue(null);
            machineMoney.setText(String.format("Koneessa: %.2f€", bd.getMoney()));
        } catch (NullPointerException e) {
            textarea.setText("Et valinnut ostettavaa virkoketta.\n");
        }
    }

    @FXML
    private void addMoney() {
        double money = moneySlider.getValue();
        if (money > 0) {
            String output = String.format("%s%s", textarea.getText(),bd.addMoney(money));
            textarea.setText(output);
            moneySlider.setValue(0);
            sliderValue.setText("0.00 €");
            machineMoney.setText(String.format("Koneessa: %.2f€", bd.getMoney()));
        } else 
            textarea.setText("Et valinnut syötettävää rahamäärää.\n");
    }
    @FXML
    private void returnMoney() {
        String output = String.format("%s%s", textarea.getText(),bd.returnMoney());
        textarea.setText(output);
        machineMoney.setText(String.format("Koneessa: %.2f€", bd.getMoney()));
    }
    @FXML
    private void printBottles() {
        String bottles = bd.printBottles();
        String output = String.format("Automaatissa olevat pullot:\n\n%s\n", bottles);
        textarea.setText(output);
    }
    
    @FXML
    public void EnterKey(KeyEvent enter) {
        if (enter.getCode().equals(KeyCode.ENTER)) {
            BuyBottle();
        }
    }

    @FXML
    private void exit() throws IOException {
        System.exit(0);
    }
    @FXML
    private void sliderChange() {
        sliderValue.setText(String.format("%.2f €", moneySlider.getValue()));
    }
    @FXML
    private void saveReturns() throws IOException {
        textarea.setText(bd.saveReturns());
    }



}
