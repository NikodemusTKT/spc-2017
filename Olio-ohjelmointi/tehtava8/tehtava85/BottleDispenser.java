package tehtava85;
// Creator: Teemu Tanninen
// Student id: 0508505
// Date: 31.05.17

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOError;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class BottleDispenser  {
    private static BottleDispenser instance;
    private int bottles;
    private double money;
    private ArrayList<Bottle> bottle_array;
    private ArrayList<Bottle> return_array;
    private static final String filename = "palautuskuitti.txt";

    private BottleDispenser() {
        bottle_array = new ArrayList<Bottle>();
        return_array = new ArrayList<Bottle>();
        bottles = 5;
        money = 0;
        String bb, bm; 
        double be, bs, bp;
        for (int i = 0; i<=bottles; i++) {
            if (i < 2) {
                bb = "Pepsi Max";
                bm = "Pepsi";
                be = 0.3;
                if (i < 1) {
                    bs = 0.5;
                    bp = 1.8;
                } else {
                    bs = 1.5;
                    bp = 2.2;
                }
            }
                else if (i < 4) {
                    bb = "Coca-Cola Zero";
                    bm = "Coca-Cola";
                    be = 0.4;
                    if (i < 3) {
                        bs = 0.5;
                        bp = 2.0;
                    } else {
                        bs = 1.5;
                        bp = 2.5;
                    }
                }
            else {
                bb = "Fanta Zero";
                bm = "Fanta";
                be = 0.5;
                bs = 0.5;
                bp = 1.95;
            }
            bottle_array.add(i, new Bottle(bb,bm,be,bs,bp));
        }
    }
    public void addBottle(int am, String bb, String bm, double be, double bs, double bp) {
        for (int i = 0; i < am; i++)
            bottle_array.add(new Bottle(bb,bm,be,bs,bp));
        bottles += am;
    }

    public String addMoney(double m) {
        money += m;
        return(String.format("Klink! Lisäsit %.2f€ rahaa laitteeseen!\n",m));
    }

    public String buyBottle(String brand, double size) {
        String result = "";
        if (bottles > 0) {
            for (Bottle bottle: bottle_array) {
                if (size == bottle.getSize() && brand == bottle.getBrand()) {
                    if (money <= 0 || bottle.getPrize() > money) {
                        return ("Et ole syöttänyt rahaa tarpeeksi!\n");
                    }
                    else {
                        bottles -= 1;
                        money -= bottle.getPrize();
                        addReturns(bottle);
                        removeBottle(bottle);
                        return ("KACHUNK! " + bottle.getBrand() + " tipahti masiinasta!\n");
                    }
                } else {
                    result = ("Pulloa ei löydy automaatista.\n");
                }
            }
        } else 
            return ("Kaikki pullot ovat loppuneet.\n");
        return result;
    }


    public String returnMoney() {
        String output = String.format("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€\n", money);
        money = 0;
        return output;
    }
    public void removeBottle(Bottle b) {
        bottle_array.remove(b);
    }
    public String printBottles() {
        StringBuilder sb = new StringBuilder();
        int i = 1;
        for (Bottle bottle: bottle_array ) {
            sb.append(i + ". Nimi: " + bottle.getBrand() + "\n"); 
            sb.append("\tKoko: " + bottle.getSize() + "\n"); 
            sb.append("\tHinta: " + bottle.getPrize() + "\n"); 
            i++;
        }
        return sb.toString();
    }

    public static BottleDispenser getInstance() {
        if (instance == null) {
            instance = new BottleDispenser();
        }
        return instance;
    }

    /**
     * @return the bottles
     */
    public int getBottles() {
        return bottles;
    }

    /**
     * @return the money
     */
    public double getMoney() {
        return money;
    }

    /**
     * @return the bottle_array
     */
    public ArrayList<Bottle> getBottle_array() {
        return bottle_array;
    }

    /**
     * @return the return_array
     */
    public ArrayList<Bottle> getReturn_array() {
        return return_array;
    }

    public int getAmount() {
        return bottles;
    }
    public void addReturns(Bottle bottle) {
        return_array.add(bottle);
    }
    public String saveReturns() throws IOException {
        String result = "";
        BufferedWriter output = null;
        double sum = 0;
        if (!return_array.isEmpty()) {
            try {
                File file = new File(filename);
                output = new BufferedWriter(new FileWriter(file));
                output.write("\nPALAUTUSKUITTI\n\n");
                output.write(String.join("",Collections.nCopies(50,"#")) + "\n");
                for (Bottle bottle: return_array) {
                    output.write(String.format("Nimi: %-10s\tKoko: %s\tHinta: %.2f\n", bottle.getBrand(),bottle.getSize(),bottle.getPrize()));
                    sum += bottle.getPrize();
                }
                output.write(String.join("",Collections.nCopies(50,"#")) + "\n");
                output.write(String.format("Palautukset yhteensä: %d \t \tSumma: %.2f\n", return_array.size(),sum));
                result = String.format("Palautukset tallennettiin onnistuneesti %s tiedostoon.\n", filename);
            } catch (IOError e) {
                result = ("Tiedoston kirjoitus epäonnistui.\n");
            } finally {
                if (output != null) 
                    output.close(); 
            }
        } else {
            result = ("Palautuksia ei löytynyt yhtään.\n");
        }
        return result;
    }
}

 


