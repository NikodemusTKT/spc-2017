/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 05.06.17                                                              ****/
/**** Exercise: 6-1                                                               ****/
/*************************************************************************************/
package tehtava61;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

abstract class Account  {
    protected String ac_num;
    protected int money;
    public Account () {}
    public Account (BufferedReader stdin) throws IOException {
        ac_num = this.GetAccountNumber(stdin,"Syötä tilinumero: ");
        money = this.GetMoney(stdin,"Syötä rahamäärä: ");
    }
    public void printAll() {
        System.out.println("Tilinumero: " + ac_num);
        System.out.println("Rahamäärä: " + money);
    }
    public int GetMoney (BufferedReader stdin,String message) throws IOException{
        boolean invalid = true;
        String input = "";
        int money = 0;
        do {
            System.out.print(message);
             try {
                 input = stdin.readLine();
                 money = Integer.parseInt(input);
                 invalid = false;
             } catch (NumberFormatException ie) {
                 System.out.println("Vain kokonaisluvut kelpaavat.");
             }
        } while (invalid);
        return money;
    }
    public String GetAccountNumber(BufferedReader stdin, String message) throws IOException {
        String num = "";
        boolean invalid = true;
        while (invalid) {
            System.out.print(message);
            num = stdin.readLine();
            if (!num.trim().isEmpty() && num.matches("^\\d+\\-\\d+") || num.matches("\\d+")) {
                invalid = false;
            }
            else {
                System.out.println("Annettu tilinumero ei kelpaa");
            }
        }
        return num;
    }
    public void RemoveAccount(BufferedReader stdin) throws IOException {
        String rm_account = this.GetAccountNumber(stdin,"Syötä poistettava tilinumero: ");
        System.out.println("Tilinumero: " + rm_account);
    }
    public void printAccount(BufferedReader stdin) throws IOException {
        String print_account = this.GetAccountNumber(stdin,"Syötä tulostettava tilinumero: ");
        System.out.println("Tilinumero: " + print_account);
    }

}

class Credit extends Account{ 
    private int c_limit;

    public Credit() {};
    public Credit(BufferedReader stdin) throws IOException{
        super(stdin);
        c_limit = this.GetCreditLimit(stdin);
    }
    public void printAll() {
        super.printAll();
        System.out.println("Luotto: " + c_limit);
    }

    public int GetCreditLimit (BufferedReader stdin) throws IOException{
        int climit = super.GetMoney(stdin,"Syötä luottoraja: ");
        return climit;
    }
}

class Normal extends Account {
    public Normal() {}
    public Normal(BufferedReader stdin) throws  IOException{
        super(stdin);
    }
}


public class Mainclass {
    public static void main (String[] args) throws IOException {
        Account credit_account, normal_account;
        BufferedReader stdin = null;
        int choice;
        while(true) {
            System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
            System.out.println("1) Lisää tavallinen tili");
            System.out.println("2) Lisää luotollinen tili");
            System.out.println("3) Tallenna tilille rahaa");
            System.out.println("4) Nosta tililtä");
            System.out.println("5) Poista tili");
            System.out.println("6) Tulosta tili");
            System.out.println("7) Tulosta kaikki tilit");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: "); 
            stdin = new BufferedReader(new InputStreamReader(System.in));
            try {
                choice = Integer.parseInt(stdin.readLine());
                if (choice == 0)
                    break;
                else if (choice == 1) {
                    normal_account = new Normal(stdin);
                    normal_account.printAll();
                    }
                else if (choice == 2) {
                    credit_account = new Credit(stdin);
                    credit_account.printAll();
                }
                else if (choice == 3) {
                    Account addMoney;
                    addMoney = new Normal(stdin);
                    addMoney.printAll();
                    }
                else if (choice == 4) {
                    Account withdraw;
                    withdraw = new Normal(stdin);
                    withdraw.printAll();
                    }
                else if (choice == 5) {
                    Account rm_account = new Normal();
                    rm_account.RemoveAccount(stdin);
                    }
                else if (choice == 6) {
                    Account print_account;
                    print_account = new Normal();
                    print_account.printAccount(stdin);
                    }
                else if (choice == 7) {
                    System.out.println("Tulostaa kaiken");
                    }
                else {
                    System.out.println("Valinta ei kelpaa.");
                }
            } catch (NumberFormatException ex) {
                System.out.println("Valinta ei ollut kokonaisluku!");
            }
        }
    }
} 
