/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 05.06.17                                                              ****/
/**** Exercise: 6-4                                                               ****/
/*************************************************************************************/
package tehtava64;

import java.io.BufferedReader;
import java.io.IOException;

abstract class Account {
    protected String ac_num; 
    protected int money;
    public Account() {}
    public Account (BufferedReader stdin) throws IOException {
        Bank input = new Bank();
        ac_num = input.GetAccountNumberInput(stdin,"Syötä tilinumero: ");
        money = input.GetMoneyInput(stdin,"Syötä rahamäärä: ");
    }
    public String getAccountNumber() {
        return ac_num;
    }
    public void setMoney(int add) {
        if (money + add > 0) {
            money += add;
        }
    }
    public void print() {
        System.out.println("Tilinumero: " + ac_num + " Tilillä rahaa: " + money);
    }
}

class NormalAccount extends Account {
    public NormalAccount (BufferedReader stdin) throws IOException {
        super(stdin);
    }
}

class CreditAccount extends Account {
    private int c_limit;
    public CreditAccount (BufferedReader stdin) throws IOException {
        super(stdin);
        Bank input = new Bank();
        c_limit = input.GetMoneyInput(stdin,"Syötä luottoraja: ");
    }
    public void print() {
        System.out.println("Tilinumero: " + ac_num + " Tilillä rahaa: " + money + " Luottoraja: " + c_limit);
    }

}


