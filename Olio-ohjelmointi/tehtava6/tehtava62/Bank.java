/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 05.06.17                                                              ****/
/**** Exercise: 6-2                                                               ****/
/*************************************************************************************/
package tehtava62;

import java.io.BufferedReader;
import java.io.IOException;

public class Bank  {
    private String ac_num;
    private int money;
    private int c_limit;

    public Bank () {}
    void addNormalAccount(BufferedReader stdin) throws IOException {
        ac_num = this.GetAccountNumber(stdin,"Syötä tilinumero: ");
        money = this.GetMoney(stdin,"Syötä rahamäärä: ");
        System.out.println("Pankkiin lisätään: " + ac_num + "," + money);
    }
    void addCreditAccount(BufferedReader stdin) throws IOException {
        ac_num = this.GetAccountNumber(stdin,"Syötä tilinumero: ");
        money = this.GetMoney(stdin,"Syötä rahamäärä: ");
        c_limit = this.GetMoney(stdin,"Syötä luottoraja: ");
        System.out.println("Pankkiin lisätään: " + ac_num + "," + money + "," + c_limit);
    }
    public int GetMoney (BufferedReader stdin,String message) throws IOException{
        boolean invalid = true;
        String input = "";
        int money = 0;
        do {
            System.out.print(message);
             try {
                 input = stdin.readLine();
                 money = Integer.parseInt(input);
                 invalid = false;
             } catch (NumberFormatException ie) {
                 System.out.println("Vain kokonaisluvut kelpaavat.");
             }
        } while (invalid);
        return money;
    }
    public String GetAccountNumber(BufferedReader stdin, String message) throws IOException {
        String num = "";
        boolean invalid = true;
        while (invalid) {
            System.out.print(message);
            num = stdin.readLine();
            if (!num.trim().isEmpty() && num.matches("^\\d+\\-\\d+") || num.matches("\\d+")) {
                invalid = false;
            }
            else {
                System.out.println("Annettu tilinumero ei kelpaa");
            }
        }
        return num;
    }
    public void RemoveAccount(BufferedReader stdin) throws IOException {
        String rm_account = this.GetAccountNumber(stdin,"Syötä poistettava tilinumero: ");
        System.out.println("Tili poistettu.");
    }
    public void SearchAccount(BufferedReader stdin) throws IOException {
        String search_account = this.GetAccountNumber(stdin,"Syötä tulostettava tilinumero: ");
        System.out.println("Etsitään tiliä: " + search_account);
    }
    public void DepositMoney(BufferedReader stdin) throws IOException {
        String account_num = this.GetAccountNumber(stdin,"Syötä tilinumero: ");
        int deposit_money = this.GetMoney(stdin,"Syötä rahamäärä: ");
        System.out.println("Talletetaan tilille: " + account_num + " rahaa " + deposit_money );
    }
    public void WithdrawMoney(BufferedReader stdin) throws IOException {
        String account_num = this.GetAccountNumber(stdin,"Syötä tilinumero: ");
        int withdraw_money = this.GetMoney(stdin,"Syötä rahamäärä: ");
        System.out.println("Nostetaan tililtä: " + account_num + " rahaa " + withdraw_money );
    }
    public void printAll() {
        System.out.println("Kaikki tilit:");
   }

}

