/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 05.06.17                                                              ****/
/**** Exercise: 6-5                                                               ****/
/*************************************************************************************/
package tehtava65;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

public class Bank  {
    private ArrayList<Account> accounts;

    public Bank() {}
    public Bank (BufferedReader stdin) {
        accounts = new ArrayList<Account>();
    }
    void addNormalAccount(BufferedReader stdin) throws IOException {
        Account n_account = new NormalAccount(stdin);
        accounts.add(n_account);
        System.out.println("Tili luotu.");
    }
    void addCreditAccount(BufferedReader stdin) throws IOException {
        Account c_account = new CreditAccount(stdin);
        accounts.add(c_account);
        System.out.println("Tili luotu.");
    }
    public void RemoveAccount(BufferedReader stdin) throws IOException {
        String rm_account = this.GetAccountNumberInput(stdin,"Syötä poistettava tilinumero: ");
        ArrayList<Account> toBeDeleted = new ArrayList<Account>();
        for (Account account: accounts) {
            if (account.getAccountNumber().equals(rm_account)) {
                toBeDeleted.add(account);
            }
        }
        if (!toBeDeleted.isEmpty()) {
            accounts.removeAll(toBeDeleted);
            System.out.println("Tili poistettu.");
        } else {
            System.out.println("Poistettavaa tiliä ei löytynyt.");
        }
    }


    public void SearchAccount(BufferedReader stdin) throws IOException {
        String search_account = this.GetAccountNumberInput(stdin,"Syötä tulostettava tilinumero: ");
        boolean found = false;
        for (Account account : accounts) {
            if (account.getAccountNumber().equals(search_account)) {
                account.print();
                found = true;
            }
        }
        if (!found) {
            System.out.println("Tulostettavaa tiliä ei löytynyt.");
        }
    }

    public void DepositMoney(BufferedReader stdin) throws IOException {
        String account_num = this.GetAccountNumberInput(stdin,"Syötä tilinumero: ");
        int deposit_money = this.GetMoneyInput(stdin,"Syötä rahamäärä: ");
        boolean found = false;
        for (Account account: accounts) {
            if (account.getAccountNumber().equals(account_num)) {
                account.setMoney(deposit_money);
                found = true;
            }
        }
        if (!found) {
            System.out.println("Talletettavaa tiliä ei löytynyt.");
        }
    }
    public void WithdrawMoney(BufferedReader stdin) throws IOException {
        String account_num = this.GetAccountNumberInput(stdin,"Syötä tilinumero: ");
        int withdraw_money = this.GetMoneyInput(stdin,"Syötä rahamäärä: ");
        boolean found = false;
        for (Account account: accounts) {
            if (account.getAccountNumber().equals(account_num)) {
                account.setMoney(-withdraw_money);
                found = true;
            }
        }
        if (!found) {
            System.out.println("Nostettavaa tiliä ei löytynyt.");
        }
    }
    public void printAll() {
        System.out.println("Kaikki tilit:");
        for (Account account : accounts) {
            account.print();
        }
   }
    public int GetMoneyInput (BufferedReader stdin,String message) throws IOException{
        boolean invalid = true;
        String input = "";
        int money = 0;
        do {
            System.out.print(message);
             try {
                 input = stdin.readLine();
                 money = Integer.parseInt(input);
                 invalid = false;
             } catch (NumberFormatException ie) {
                 System.out.println("Vain kokonaisluvut kelpaavat.");
             }
        } while (invalid);
        return money;
    }
    public String GetAccountNumberInput(BufferedReader stdin, String message) throws IOException {
        String num = "";
        boolean invalid = true;
        while (invalid) {
            System.out.print(message);
            num = stdin.readLine();
            if (!num.trim().isEmpty() && num.matches("^\\d+\\-\\d+") || num.matches("\\d+")) {
                invalid = false;
            }
            else {
                System.out.println("Annettu tilinumero ei kelpaa");
            }
        }
        return num;
    }

}

