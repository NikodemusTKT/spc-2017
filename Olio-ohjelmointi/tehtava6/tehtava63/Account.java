/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 02.06.102.06.17                                                       ****/
/**** Exercise: 6-3                                                               ****/
/*************************************************************************************/
package tehtava64;

import java.io.BufferedReader;
import java.io.IOException;

public abstract class Account {
    protected String ac_num; 
    protected int money;
    public Account() {}
    public Account (BufferedReader stdin) throws IOException {
        ac_num = this.GetAccountNumberInput(stdin,"Syötä tilinumero: ");
        money = this.GetMoney(stdin,"Syötä rahamäärä: ");
    }
    public int GetMoney (BufferedReader stdin,String message) throws IOException{
        boolean invalid = true;
        String input = "";
        int money = 0;
        do {
            System.out.print(message);
             try {
                 input = stdin.readLine();
                 money = Integer.parseInt(input);
                 invalid = false;
             } catch (NumberFormatException ie) {
                 System.out.println("Vain kokonaisluvut kelpaavat.");
             }
        } while (invalid);
        return money;
    }
    public String GetAccountNumberInput(BufferedReader stdin, String message) throws IOException {
        String num = "";
        boolean invalid = true;
        while (invalid) {
            System.out.print(message);
            num = stdin.readLine();
            if (!num.trim().isEmpty() && num.matches("^\\d+\\-\\d+") || num.matches("\\d+")) {
                invalid = false;
            }
            else {
                System.out.println("Annettu tilinumero ei kelpaa");
            }
        }
        return num;
    }
    public String getAccountNumber() {
        return ac_num;
    }
    public void setMoney(int add) {
        if (money + add > 0) {
            money += add;
        }
    }
    public void print() {
        System.out.println("Tilinumero: " + ac_num + " Tilillä rahaa: " + money);
    }
}

class NormalAccount extends Account {
    public NormalAccount (BufferedReader stdin) throws IOException {
        super(stdin);
        System.out.println("Tili luotu.");
    }
}

class CreditAccount extends Account {
    private int c_limit;
    public CreditAccount (BufferedReader stdin) throws IOException {
        super(stdin);
        c_limit = super.GetMoney(stdin,"Syötä luottoraja: ");
        System.out.println("Tili luotu.");
    }
}


