/*************************************************************************************/
/**** Author: Teemu Tanninen                                                      ****/
/**** Student id: 0508505                                                         ****/
/**** Date: 05.06.17                                                              ****/
/**** Exercise: 6-3                                                               ****/
/*************************************************************************************/
package tehtava63;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;



public class Mainclass {
    public static void main (String[] args) throws IOException {
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        Bank acc = new Bank(stdin);
        int choice;
        while(true) {
            System.out.println("\n*** PANKKIJÄRJESTELMÄ ***");
            System.out.println("1) Lisää tavallinen tili");
            System.out.println("2) Lisää luotollinen tili");
            System.out.println("3) Tallenna tilille rahaa");
            System.out.println("4) Nosta tililtä");
            System.out.println("5) Poista tili");
            System.out.println("6) Tulosta tili");
            System.out.println("7) Tulosta kaikki tilit");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: "); 
            try {
                choice = Integer.parseInt(stdin.readLine());
                if (choice == 0)
                    break;
                else if (choice == 1) {
                    acc.addNormalAccount(stdin);
                    }
                else if (choice == 2) {
                    acc.addCreditAccount(stdin);
                }
                else if (choice == 3) {
                    acc.DepositMoney(stdin);
                    }
                else if (choice == 4) {
                    acc.WithdrawMoney(stdin);
                    }
                else if (choice == 5) {
                    acc.RemoveAccount(stdin);
                    }
                else if (choice == 6) {
                    acc.SearchAccount(stdin);
                    }
                else if (choice == 7) {
                    acc.printAll();
                    }
                else {
                    System.out.println("Valinta ei kelpaa.");
                }
            } catch (NumberFormatException ex) {
                System.out.println("Valinta ei ollut kokonaisluku!");
            }
        }
    }
} 
