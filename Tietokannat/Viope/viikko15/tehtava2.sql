/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 27.06.17             **/
/********************************/
SELECT DISTINCT Kustantaja FROM VarastossaOlevatKirjat
INNER JOIN Helkala_Maarit_Kirjat ON VarastossaOlevatKirjat.Kirja = Helkala_Maarit_Kirjat.Kirja;
