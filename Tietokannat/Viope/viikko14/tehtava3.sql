/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 27.06.17             **/
/********************************/

SELECT "kirjailija"."etunimi", "kirjailija"."sukunimi", COUNT(*) AS "kirjoitettuja kirjoja" FROM 
    "kirja" LEFT OUTER JOIN "kirjailija" ON "kirjailija"."tekijaid" = "kirja"."tekijaid";
    ORDER BY "kirjailija"."sukunimi", "kirjailija"."etunimi" ASC;


