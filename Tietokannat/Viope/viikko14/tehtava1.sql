/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 27.06.17             **/
/********************************/

SELECT "kirja"."nimi" as "kirja", "kustantaja"."nimi" AS "kustantaja"
    FROM ("kirja" INNER JOIN "kustantaja" ON "kirja"."kustantajaid" = "kustantaja"."kustantajaid")
    INNER JOIN "varasto" ON "varasto"."kirjaid" = "kirja"."kirjaid"
	WHERE "varasto"."lukumaara" > 0
	ORDER BY "kirja"."nimi";
