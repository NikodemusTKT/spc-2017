/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 27.06.17             **/
/********************************/

SELECT "tekijaid", SUM("sivuja") AS "Kirjoitettuja sivuja yhteensä" FROM "kirja"
    GROUP BY "tekijaid" 
    ORDER BY "tekijaid" ASC;
