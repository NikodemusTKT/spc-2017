/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 27.06.17             **/
/********************************/

SELECT "tekijaid", 'kirjoittamien kirjojen lukumäärä on' as "selite", COUNT(*) FROM "kirja"
    GROUP BY "tekijaid" 
    ORDER BY "tekijaid" ASC;
