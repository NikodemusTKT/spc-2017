/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 27.06.17             **/
/********************************/

SELECT 'Kirjassa' AS "selite1",  "kirjaid" AS "kirja", 'on yli 200 sivua' as "selite2" FROM "kirja"
    GROUP BY "kirjaid" 
    HAVING SUM("sivuja") > 200
    ORDER BY "kirjaid" ASC;
