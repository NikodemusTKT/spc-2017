/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 26.06.17             **/
/********************************/

SELECT nimi, hinta, sivuja, 
FROM kirja
WHERE julkaisupvm < '1995-01-01'
ORDER BY nimi;
