
/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 26.06.17             **/
/********************************/
CREATE TABLE KURSSI
("kurssinro" INTEGER PRIMARY KEY NOT NULL,
    "nimi" VARCHAR(32) NOT NULL ,
    "alkamispvm" date
);
