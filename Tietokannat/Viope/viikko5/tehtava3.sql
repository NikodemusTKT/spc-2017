
/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 26.06.17             **/
/********************************/


CREATE TABLE SUORITUS (
    "opiskelijanro" INTEGER  NOT NULL,
    "kurssinro" INTEGER  NOT NULL,
    PRIMARY KEY (opiskelijanro,kurssinro),
    FOREIGN KEY (opiskelijanro) REFERENCES Opiskelija(Opiskelijanro),
    FOREIGN KEY (kurssinro) REFERENCES Kurssi(Kurssinro),
    "arvosana" INTEGER NOT NULL
);
