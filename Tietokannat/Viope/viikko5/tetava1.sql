
/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 26.06.17             **/
/********************************/
CREATE TABLE OPISKELIJA
    ("opiskelijanro" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
        "etunimi"   VARCHAR(32) NOT NULL ,
        "sukunimi"  VARCHAR(60) NOT NULL ,
        "osoite"    VARCHAR(100) ,
        "puhnro"    VARCHAR(15)
);
