/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 26.06.17             **/
/********************************/
SELECT hinta, nimi FROM kirja
WHERE hinta > 30
ORDER BY "hinta" DESC;
