/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 26.06.17             **/
/********************************/
SELECT DISTINCT TekijaID 
   FROM Kirja
   ORDER BY TekijaID ASC;
