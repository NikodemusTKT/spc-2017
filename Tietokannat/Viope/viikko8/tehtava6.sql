/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 26.06.17             **/
/********************************/
SELECT etunimi,sukunimi FROM kirjailija
    ORDER BY sukunimi, etunimi;
