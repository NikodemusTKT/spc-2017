/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 26.06.17             **/
/********************************/

SELECT nimi, sivuja, hinta, julkaisupvm FROM Kirja
WHERE "sivuja" < 500 AND "hinta" > 20;
