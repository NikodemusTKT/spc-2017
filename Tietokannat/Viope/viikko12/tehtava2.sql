/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 27.06.17             **/
/********************************/

SELECT "tekijaid", AVG("hinta"), AVG("sivuja") FROM "kirja" GROUP BY "tekijaid" ORDER BY "tekijaid" DESC;
