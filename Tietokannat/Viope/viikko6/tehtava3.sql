/********************************/
/** Author: Teemu Tanninen     **/
/** Student.id: 0508505        **/
/** Date: 26.06.17             **/
/********************************/

UPDATE KURSSI 
SET Alkamispvm = '2008-1-16', Vastuuhenkilo = 'Saarinen Risto'
WHERE Kurssinro = 1021 AND Nimi = 'Tietokantojen perusteet';
