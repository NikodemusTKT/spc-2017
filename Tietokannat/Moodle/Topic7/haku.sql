EXPLAIN QUERY PLAN SELECT COUNT(*) 
    FROM (koira INNER JOIN koirat_kurssit ON koira.kotunnus = koirat_kurssit.kotunnus)
    INNER JOIN kurssi ON koirat_kurssit.kutunnus = kurssi.kutunnus
    INNER JOIN kouluttaja on kurssi.kltunnus = kouluttaja.kltunnus
    WHERE koira.rotu = "Snautseri" AND kouluttaja.etunimi = "Sanna";
