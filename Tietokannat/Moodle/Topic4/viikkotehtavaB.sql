CREATE TABLE Kurssi(
    "Kurssitunnus" VARCHAR(30) PRIMARY KEY NOT NULL,
    "Henkkuntatunnus" VARCHAR(30) NOT NULL,
    "YliopistoNimi" VARCHAR(30) NOT NULL,
    "Kaupunki" VARCHAR(30) NOT NULL,
    "Nimi" VARCHAR(30),
    "Pvm" DATE,
    FOREIGN KEY (Henkkuntatunnus) REFERENCES Opettaja(Henkkuntatunnus),
    FOREIGN KEY (Kurssitunnus) REFERENCES Opiskelee(Kurssitunnus),
    FOREIGN KEY (YliopistoNimi, Kaupunki) REFERENCES Yliopisto(YliopistoNimi, Kaupunki)
);

CREATE TABLE Yliopisto(
    "YliopistoNimi" VARCHAR(30) NOT NULL,
    "Kaupunki" VARCHAR(30) NOT NULL,
    "Katuosoite" VARCHAR(30),
    PRIMARY KEY (YliopistoNimi, Kaupunki)
);

CREATE TABLE Opiskelee(
    "Opiskelijatunnus" INTEGER NOT NULL,
    "Kurssitunnus" VARCHAR(30) NOT NULL,
    PRIMARY KEY(Opiskelijatunnus, Kurssitunnus),
    FOREIGN KEY(Opiskelijatunnus) REFERENCES Opiskelija(Opiskelijatunnus)
);

CREATE TABLE Opiskelija(
    "Opiskelijatunnus" INTEGER PRIMARY KEY NOT NULL,
    "Nimi" VARCHAR(30) NOT NULL,
    "Email" VARCHAR(30)
);


CREATE TABLE Opettaja(
    "Henkkuntatunnus" VARCHAR(30) PRIMARY KEY NOT NULL,
    "Email" VARCHAR(30)
);
