## Tehtävä 1
```sql
SELECT * FROM omistaja WHERE koiravero_maksettu = 0;
omtunnus    etunimi     sukunimi     asuinpaikkakunta  puhelinnumero  koiravero_maksettu
----------  ----------  -----------  ----------------  -------------  ------------------
4           Hannes      Heikalainen  Helsinki          1234557        0                 
```

---

## Tehtävä 2
```sql
SELECT omistaja.*, 'omistaa koiran' AS "Omistus", koira.koitunnus, koira.nimi, koira.ika FROM omistaja 
INNER JOIN koira ON omistaja.omtunnus = koira.omtunnus;
omtunnus    etunimi     sukunimi    asuinpaikkakunta  puhelinnumero  koiravero_maksettu  Omistus         koitunnus   nimi        ika       
----------  ----------  ----------  ----------------  -------------  ------------------  --------------  ----------  ----------  ----------
1           Janne       Jokinen     Lappeenranta      1234555        1                   omistaa koiran  1           Vuhveli     5         
1           Janne       Jokinen     Lappeenranta      1234555        1                   omistaa koiran  2           Hauhau      13        
1           Janne       Jokinen     Lappeenranta      1234555        1                   omistaa koiran  3           Applebay    4         
3           Hannes      Heikalaine  Helsinki          1234557        0                   omistaa koiran  4           Musti       7         
```
                                                                                                                                                    
******

## Tehtävä 3
```sql
INSERT INTO omistaja VALUES (4, 'Olga', 'Ainahiljaa', 'Espoo', '0406677', 1);
INSERT INTO koira (omtunnus, nimi, ika) VALUES (4, 'Vuhveli', 10);
INSERT INTO koira (omtunnus, nimi, ika) VALUES (4, 'Doge', 2);
SELECT omistaja.*, 'omistaa koiran' AS "Omistus", koira.koitunnus, koira.nimi, koira.ika FROM omistaja 
INNER JOIN koira ON omistaja.omtunnus = koira.omtunnus;

omtunnus    etunimi     sukunimi    asuinpaikkakunta  puhelinnumero  koiravero_maksettu  Omistus         koitunnus   nimi        ika
----------  ----------  ----------  ----------------  -------------  ------------------  --------------  ----------  ----------  ----------
1           Janne       Jokinen     Lappeenranta      1234555        1                   omistaa koiran  1           Vuhveli     5
1           Janne       Jokinen     Lappeenranta      1234555        1                   omistaa koiran  2           Hauhau      13
1           Janne       Jokinen     Lappeenranta      1234555        1                   omistaa koiran  3           Applebay    4
3           Hannes      Heikalaine  Helsinki          1234557        0                   omistaa koiran  4           Musti       7
4           Olga        Ainahiljaa  Espoo             0406677        1                   omistaa koiran  5           Vuhveli     10
4           Olga        Ainahiljaa  Espoo             0406677        1                   omistaa koiran  6           Doge        2
```

---

## Tehtävä 4
```sql
UPDATE omistaja  SET koiravero_maksettu = 0 WHERE asuinpaikkakunta = 'Lappeenranta';
SELECT * FROM omistaja;
 omtunnus    etunimi     sukunimi    asuinpaikkakunta  puhelinnumero  koiravero_maksettu
 ----------  ----------  ----------  ----------------  -------------  ------------------
 1           Janne       Jokinen     Lappeenranta      1234555        0
 2           Matti       Meikalaine  Lappeenranta      1244556        0
 3           Hannes      Heikalaine  Helsinki          1234557        0
 4           Olga        Ainahiljaa  Espoo             0406677        1
```

