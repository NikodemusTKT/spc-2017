CREATE TABLE omistaja

("omtunnus" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , 

"etunimi" VARCHAR(10), 

"sukunimi" VARCHAR(10), 

"asuinpaikkakunta" VARCHAR(20), 

"puhelinnumero" VARCHAR(10), 

"koiravero_maksettu" BOOL);

    

CREATE  TABLE koira ("koitunnus" INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL ,

 "omtunnus" INTEGER NOT NULL ,

  "nimi" VARCHAR(10), 

  "ika" INTEGER);



INSERT INTO omistaja

(etunimi, sukunimi, asuinpaikkakunta, puhelinnumero, koiravero_maksettu)

VALUES

('Janne', 'Jokinen', 'Lappeenranta', '1234555', 1);



INSERT INTO omistaja

(etunimi, sukunimi, asuinpaikkakunta, puhelinnumero, koiravero_maksettu)

VALUES

('Matti', 'Meikalainen', 'Lappeenranta', '1244556', 1);



INSERT INTO omistaja

(etunimi, sukunimi, asuinpaikkakunta, puhelinnumero, koiravero_maksettu)

VALUES

('Hannes', 'Heikalainen', 'Helsinki', '1234557', 0);



INSERT INTO koira

(omtunnus, nimi, ika)

VALUES

(1, 'Vuhveli', 5);



INSERT INTO koira

(omtunnus, nimi, ika)

VALUES

(1, 'Hauhau', 13);



INSERT INTO koira

(omtunnus, nimi, ika)

VALUES

(1, 'Applebay', 4);



INSERT INTO koira

(omtunnus, nimi, ika)

VALUES

(3, 'Musti', 7);
