/* Tekijä: Teemu Tanninen */
/* Pvm: 04.07.17 */
/* Kurssi: CT60A4302 Tietokannat, kesäkurssi 2017 */
/* Tehtävä: Topic 5 - Viikkotehtävä: Taulukon normalisointi */
/* Kuvaus: Relaatiotietokanta, joka on kolmannessa normaalimuodossa */

CREATE TABLE Omistajataulu (
    Omtunnus INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    Etunimi VARCHAR(10),
    Sukunimi VARCHAR(10),
    Osoite Varchar(20),
    Postinumero INTEGER NOT NULL,
    FOREIGN KEY (Omtunnus) REFERENCES Koirataulu(Omtunnus),
    FOREIGN KEY (Postinumero) REFERENCES Postinumerotaulu(Postinumero)
);

CREATE TABLE Postinumerotaulu (
    Postinumero INTEGER PRIMARY KEY NOT NULL,
    Postitoimipaikka VARCHAR(20) NOT NULL
);

CREATE TABLE Koirataulu (
    Koitunnus INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    Omtunnus INTEGER NOT NULL,
    Nimi VARCHAR(10),
    Koirarotu VARCHAR(20),
    Koirakoulussa VARCHAR(20) DEFAULT 'Ei ole'
);

INSERT INTO Omistajataulu(Etunimi, Sukunimi, Osoite, Postinumero) 
VALUES ('Matti', 'Meikalainen', 'Skinnarilankatu 34', 53850);

INSERT INTO Omistajataulu(Etunimi, Sukunimi, Osoite, Postinumero)
VALUES ('Maija', 'Teikalainen', 'Skinnarilankatu 34', 53850);

INSERT INTO Koirataulu (Omtunnus, Nimi, Koirarotu)
VALUES (1,'Musti', 'Seka');

INSERT INTO Koirataulu (Omtunnus, Nimi, Koirarotu)
VALUES (1,'Tapla', 'seka');

INSERT INTO Koirataulu (Omtunnus, Nimi, Koirarotu)
VALUES (1,'Pilkku', 'Leonberg');

INSERT INTO Koirataulu (Omtunnus, Nimi, Koirarotu)
VALUES (1,'Huski', 'Berninpaimenkoira');

INSERT INTO Koirataulu (Omtunnus, Nimi, Koirarotu, Koirakoulussa)
VALUES (1,'Tapla', 'suomenpystykorva', 'Tottelevaisuuskoulussa');

INSERT INTO Koirataulu (Omtunnus, Nimi, Koirarotu, Koirakoulussa)
VALUES (1,'Pilkku', 'suomenpystykorva', 'Tottelevaisuuskoulussa');

INSERT INTO Koirataulu (Omtunnus, Nimi, Koirarotu, Koirakoulussa)
VALUES (1,'Huski', 'suomenpystykorva', 'Tottelevaisuuskoulussa');


INSERT INTO Koirataulu (Omtunnus, Nimi, Koirarotu)
VALUES (2,'Wuffe', 'Seka');

INSERT INTO Koirataulu (Omtunnus, Nimi, Koirarotu)
VALUES (2,'Tapla', 'Corgi');

INSERT INTO Koirataulu (Omtunnus, Nimi, Koirarotu)
VALUES (2,'Tapla', 'Corgi');

INSERT INTO Koirataulu (Omtunnus, Nimi, Koirarotu, Koirakoulussa)
VALUES (2,'Musti', 'suomenpystykorva', 'Tottelevaisuuskoulussa');

INSERT INTO Postinumerotaulu
VALUES (53850, 'Lappeenranta');
