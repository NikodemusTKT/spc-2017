'''
Tekijä: Teemu Tanninen
OP.numero: 0508505
Pvm: 04.07.17
Kurssi: CT60A4302 Tietokannat, kesäkurssi 2017
Tehtävä: Topic 6 - Viikkotehtävä: Sqlite3 ja Python
'''
import sqlite3

# Create sqlite database connection
conn = sqlite3.connect('koirakoulu.sqlite3')

# Create cursor object
c = conn.cursor()

# Ask for user input for searching database

nimi = input("Anna koiran nimi: ")
tuple = (nimi,)

# Execute search (Select) on database
c.execute('''SELECT koira.kotunnus,koira.omtunnus,koira.ika,koira.rotu,
          omistaja.etunimi, omistaja.asuinpaikkakunta,
          kurssi.*,
          kouluttaja.*
          FROM (koira
          LEFT JOIN omistaja ON koira.omtunnus = omistaja.omtunnus)
          LEFT JOIN koirat_kurssit ON koira.kotunnus = koirat_kurssit.kotunnus
          LEFT JOIN kurssi ON koirat_kurssit.kutunnus = kurssi.kutunnus
          LEFT JOIN kouluttaja ON kurssi.kltunnus = kouluttaja.kltunnus
          WHERE nimi=?
          ORDER BY omistaja.omtunnus;''', tuple)


# Fetch all data which matched the selection
data = c.fetchall()

# Print all the fetched data from the database
print("Koiran {} tiedot...".format(nimi))
for row in data:
    ktunnus = row[0]
    omtunnus = row[1]
    ika = row[2]
    rotu = row[3]
    omnimi = row[4]
    paikka = row[5]
    kurssi = row[6:10]
    kurssitiedot = "kutunnus: {d[0]}, kltunnus: {d[1]}, kurssinimi: {d[2]}, pvm: {d[3]}"
    koulut = row[10:14]
    kouluttiedot = "kltunnus: {d[0]}, etunimi: {d[1]}, sukunimi: {d[2]}, ika: {d[3]}"

    print("""
    ===========================

    Koiran tiedot
    ---------------------------
    kotunnus: {0}, omtunnus: {1}, ikä: {2}, rotu: {3}

    Omistajan tiedot:
    ---------------------------
    nimi: {4}, paikkakunta: {5}

    Kurssitiedot
    ---------------------------
    {6}

    Kouluttajan tiedot
    ---------------------------
    {7}
    """.format(ktunnus, omtunnus, ika, rotu, omnimi, paikka,
        kurssitiedot.format(d=kurssi) if kurssi[0] is  not None else "Ei kurssia",
        kouluttiedot.format(d=koulut) if koulut[0] is not None else "Ei kouluttajaa"))

conn.close()
